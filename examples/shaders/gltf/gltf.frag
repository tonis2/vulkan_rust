#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable


struct Material {
    vec4 base_color;
    vec4 color;
    vec4 emissive_color;
    vec4 emissive;
    vec4 light_settings;

    int color_texture;
    int emissive_texture;
    int normals_texture;
    int occlusion_texture;
};

layout (constant_id = 0) const uint MATERIALS_AMOUNT = 0U;
layout (constant_id = 1) const uint TEXTURE_AMOUNT = 0U;

layout (binding = 1) uniform Materials { Material content [MATERIALS_AMOUNT > 0 ? MATERIALS_AMOUNT : 1]; } materials;
layout (binding = 2) uniform sampler2D textureSampler[TEXTURE_AMOUNT > 0 ? TEXTURE_AMOUNT : 1];

layout (location = 0) in vec4 fragColor;
layout (location = 1) in vec4 tangents;
layout (location = 2) in vec3 normal;
layout (location = 3) in vec2 uv;
layout (location = 4) in flat int material_index;

layout (location = 0) out vec4 outColor;

void main() {
    if (MATERIALS_AMOUNT > 0) {
        Material mesh_material = materials.content[material_index];
        
        //Apply Material data to mesh
        if (mesh_material.color_texture != -1) {
             outColor = texture(textureSampler[mesh_material.color_texture], uv);
        }
    
    } else {
        outColor = fragColor;
    } 
}
