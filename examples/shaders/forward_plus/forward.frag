#version 450
#extension GL_ARB_separate_shader_objects : enable


#define MAX_POINT_LIGHT_PER_TILE 1023
#define TILE_SIZE 16


layout (constant_id = 0) const uint MATERIALS_AMOUNT = 0U;
layout (constant_id = 1) const uint TEXTURE_AMOUNT = 0U;
layout (constant_id = 2) const uint LIGHT_COUNT = 0U;


struct Light {
    vec3 position;
    vec3 color;
    float intensity;
	float range;
	uint  light_type;
	float inner_cone_angle;
	float outer_cone_angle;
};


struct LightVisiblity
{
	uint count;
	uint light_indices[MAX_POINT_LIGHT_PER_TILE];
};


struct Material {
    vec4 base_color;
    vec4 color;
    vec4 emissive_color;
    vec4 emissive;
    vec4 light_settings;

    int color_texture;
    int emissive_texture;
    int normals_texture;
    int occlusion_texture;
};

layout (binding = 1) uniform MaterialData {
  Material materials[MATERIALS_AMOUNT > 0 ? MATERIALS_AMOUNT : 1];
};

layout (binding = 2) uniform sampler2D textureSampler[TEXTURE_AMOUNT > 0 ? TEXTURE_AMOUNT : 1];

layout (std140, binding = 3) uniform LightBuffer {
    Light lights[LIGHT_COUNT > 0 ? LIGHT_COUNT : 1];
};

layout(std430, binding = 4) buffer readonly TileLightVisiblities
{
    LightVisiblity light_visiblities[];
};

layout (location = 0) in vec4 fragColor;
layout (location = 1) in vec3 in_tangent;
layout (location = 2) in vec3 in_normal;
layout (location = 3) in vec2 uv;
layout (location = 4) in vec4 model_postion;
layout (location = 5) in flat int material_index;

layout(location = 0) out vec4 out_color;

void main() {
    if (MATERIALS_AMOUNT > 0) {
        Material mesh_material = materials[material_index];
    
        //Apply Material data to mesh
        if (mesh_material.color_texture != -1) {
            out_color = texture(textureSampler[mesh_material.color_texture], uv);
        }

    } else {
        out_color = fragColor;
    } 
}