#!/bin/sh

compile() {
    glslangValidator -V $1 -o $1.spv
}

compile ./triangle/triangle.vert
compile ./triangle/triangle.frag
compile ./texture/quad.vert
compile ./texture/quad.frag
compile ./gltf/gltf.vert
compile ./gltf/gltf.frag

compile ./forward_plus/depth_prepass.vert
compile ./forward_plus/forward.vert
compile ./forward_plus/forward.frag
compile ./forward_plus/light_culling.comp


compile ./vxgi/gbuffer.vert
compile ./vxgi/gbuffer.frag
compile ./vxgi/forward.vert
compile ./vxgi/forward.frag
