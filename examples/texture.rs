use winit::{
    event::{
        DeviceEvent, ElementState, Event, KeyboardInput, StartCause, VirtualKeyCode, WindowEvent,
    },
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

use std::{ffi::CString, mem, path::Path, sync::Arc };
use vulkan::*;

const FRAMES_IN_FLIGHT: usize = 2;
#[derive(Clone, Debug, Copy)]
pub struct Vertex {
    pub pos: [f32; 2],
    pub color: [f32; 3],
    pub tex_coord: [f32; 2],
}

unsafe impl bytemuck::Zeroable for Vertex {}
unsafe impl bytemuck::Pod for Vertex {}

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("test")
        .with_resizable(false)
        .build(&event_loop)
        .unwrap();

    let vertices = vec![
        Vertex {
            pos: [-0.75, -0.75],
            color: [1.0, 0.0, 0.0],
            tex_coord: [1.0, 0.0],
        },
        Vertex {
            pos: [0.75, -0.75],
            color: [0.0, 1.0, 0.0],
            tex_coord: [0.0, 0.0],
        },
        Vertex {
            pos: [0.75, 0.75],
            color: [0.0, 0.0, 1.0],
            tex_coord: [0.0, 1.0],
        },
        Vertex {
            pos: [-0.75, 0.75],
            color: [1.0, 1.0, 1.0],
            tex_coord: [1.0, 1.0],
        },
    ];

    let indices = vec![0, 1, 2, 2, 3, 0];

    let context = Arc::new(Context::new("test", &window));
    let swapchain = Swapchain::new(context.clone());

    let command_pool = context.create_command_pool(
        CommandPoolCreateInfoBuilder::new().queue_family_index(context.graphics_queue_index),
    );
    let command_buffers = context.create_command_buffers(
        CommandBufferAllocateInfoBuilder::new()
            .command_pool(command_pool)
            .level(CommandBufferLevel::PRIMARY)
            .command_buffer_count(swapchain.image_count()),
    );

    let texture = Texture::from_file(
        Path::new("examples/assets/texture.jpg"),
        &context,
        command_pool,
    );

    let descriptor = Descriptor::new(
        vec![DescriptorEntry {
            bind_index: 0,
            bind_type: DescriptorType::COMBINED_IMAGE_SAMPLER,
            flag: ShaderStageFlags::FRAGMENT,
            image_info: Some(vec![DescriptorImageInfo {
                image_view: texture.view(),
                sampler: texture.sampler(),
                image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            }]),
            ..Default::default()
        }],
        context.clone(),
    );

    let layout = Pipeline::layout(
        PipelineLayoutCreateInfoBuilder::new().set_layouts(&vec![descriptor.layout()]),
        context.clone(),
    );

    let vertex_buffer = context.create_buffer(
        BufferCreateInfoBuilder::new()
            .sharing_mode(SharingMode::EXCLUSIVE)
            .usage(BufferUsageFlags::VERTEX_BUFFER)
            .size((vertices.len() * std::mem::size_of::<Vertex>()) as DeviceSize),
        bytemuck::cast_slice(&vertices),
    );

    let index_buffer = context.create_buffer(
        BufferCreateInfoBuilder::new()
            .sharing_mode(SharingMode::EXCLUSIVE)
            .usage(BufferUsageFlags::INDEX_BUFFER)
            .size((indices.len() * std::mem::size_of::<u32>()) as DeviceSize),
        bytemuck::cast_slice(&indices.clone()),
    );

    let input_binding = vec![VertexInputBindingDescriptionBuilder::new()
        .input_rate(VertexInputRate::VERTEX)
        .binding(0)
        .stride(mem::size_of::<Vertex>() as u32)];

    let input_descriptions = vec![
        VertexInputAttributeDescriptionBuilder::new()
            .location(0)
            .binding(0)
            .format(Format::R32G32_SFLOAT)
            .offset(offset_of!(Vertex, pos) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(1)
            .binding(0)
            .format(Format::R32G32B32_SFLOAT)
            .offset(offset_of!(Vertex, color) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(2)
            .binding(0)
            .format(Format::R32G32_SFLOAT)
            .offset(offset_of!(Vertex, tex_coord) as u32),
    ];
    let vertex_input = PipelineVertexInputStateCreateInfoBuilder::new()
        .vertex_binding_descriptions(&input_binding)
        .vertex_attribute_descriptions(&input_descriptions);

    // let vertex_input = PipelineVertexInputStateCreateInfoBuilder::new();
    let input_assembly = PipelineInputAssemblyStateCreateInfoBuilder::new()
        .topology(PrimitiveTopology::TRIANGLE_LIST)
        .primitive_restart_enable(false);

    let viewports = vec![ViewportBuilder::new()
        .x(0.0)
        .y(0.0)
        .width(context.extent().width as f32)
        .height(context.extent().height as f32)
        .min_depth(0.0)
        .max_depth(1.0)];
    let scissors = vec![Rect2DBuilder::new()
        .offset(Offset2D { x: 0, y: 0 })
        .extent(context.extent())];
    let viewport_state = PipelineViewportStateCreateInfoBuilder::new()
        .viewports(&viewports)
        .scissors(&scissors);

    let rasterizer = PipelineRasterizationStateCreateInfoBuilder::new()
        .depth_clamp_enable(false)
        .rasterizer_discard_enable(false)
        .polygon_mode(PolygonMode::FILL)
        .line_width(1.0)
        .cull_mode(CullModeFlags::BACK)
        .front_face(FrontFace::CLOCKWISE)
        .depth_clamp_enable(false);

    let multisampling = PipelineMultisampleStateCreateInfoBuilder::new()
        .sample_shading_enable(false)
        .rasterization_samples(SampleCountFlagBits::_1);

    let color_blend_attachments = vec![PipelineColorBlendAttachmentStateBuilder::new()
        .color_write_mask(
            ColorComponentFlags::R
                | ColorComponentFlags::G
                | ColorComponentFlags::B
                | ColorComponentFlags::A,
        )
        .blend_enable(false)];

    let color_blending = PipelineColorBlendStateCreateInfoBuilder::new()
        .logic_op_enable(false)
        .attachments(&color_blend_attachments);

    let attachments = vec![AttachmentDescriptionBuilder::new()
        .format(context.format())
        .samples(SampleCountFlagBits::_1)
        .load_op(AttachmentLoadOp::CLEAR)
        .store_op(AttachmentStoreOp::STORE)
        .stencil_load_op(AttachmentLoadOp::DONT_CARE)
        .stencil_store_op(AttachmentStoreOp::DONT_CARE)
        .initial_layout(ImageLayout::UNDEFINED)
        .final_layout(ImageLayout::PRESENT_SRC_KHR)];

    let color_attachment_refs = vec![AttachmentReferenceBuilder::new()
        .attachment(0)
        .layout(ImageLayout::COLOR_ATTACHMENT_OPTIMAL)];
    let subpasses = vec![SubpassDescriptionBuilder::new()
        .pipeline_bind_point(PipelineBindPoint::GRAPHICS)
        .color_attachments(&color_attachment_refs)];
    let dependencies = vec![SubpassDependencyBuilder::new()
        .src_subpass(SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .src_access_mask(AccessFlags::empty())
        .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_WRITE)];

    let renderpass = Renderpass::new(
        *RenderPassCreateInfoBuilder::new()
            .attachments(&attachments)
            .subpasses(&subpasses)
            .dependencies(&dependencies),
        context.clone(),
    );

    let entry_point = CString::new("main").unwrap();

    let vertex_module = Shader::build(Path::new("examples/shaders/texture/quad.vert.spv"), context.clone());
    let shader_module = Shader::build(Path::new("examples/shaders/texture/quad.frag.spv"), context.clone());

    let shader_stages = vec![
        PipelineShaderStageCreateInfoBuilder::new()
            .stage(ShaderStageFlagBits::VERTEX)
            .name(&entry_point)
            .module(vertex_module.module()),
        PipelineShaderStageCreateInfoBuilder::new()
            .stage(ShaderStageFlagBits::FRAGMENT)
            .name(&entry_point)
            .module(shader_module.module()),
    ];

    let pipeline_info = GraphicsPipelineCreateInfoBuilder::new()
        .stages(&shader_stages)
        .vertex_input_state(&vertex_input)
        .input_assembly_state(&input_assembly)
        .viewport_state(&viewport_state)
        .rasterization_state(&rasterizer)
        .multisample_state(&multisampling)
        .color_blend_state(&color_blending)
        .layout(layout.info())
        .render_pass(renderpass.info())
        .subpass(0);

    let pipelines = Pipeline::graphics_pipelines(&vec![pipeline_info], context.clone());

    // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Framebuffers
    let framebuffers: Vec<Framebuffer> = swapchain
        .images()
        .iter()
        .map(|image_view| {
            Framebuffer::new(
                *FramebufferCreateInfoBuilder::new()
                    .render_pass(renderpass.info())
                    .attachments(&vec![*image_view])
                    .width(context.extent().width as u32)
                    .height(context.extent().height as u32)
                    .layers(1),
                context.clone(),
            )
        })
        .collect();

    for (index, &command_buffer) in command_buffers.iter().enumerate() {
        let begin_info = CommandBufferBeginInfoBuilder::new();
        unsafe {
            context
                .device
                .begin_command_buffer(command_buffer, &begin_info)
        }
        .unwrap();

        let clear_values = vec![ClearValue {
            color: ClearColorValue {
                float32: [0.0, 0.0, 0.0, 1.0],
            },
        }];
        let begin_info = RenderPassBeginInfoBuilder::new()
            .render_pass(renderpass.info())
            .framebuffer(framebuffers[index].info())
            .render_area(Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: context.extent(),
            })
            .clear_values(&clear_values);

        unsafe {
            context.device.cmd_begin_render_pass(
                command_buffer,
                &begin_info,
                SubpassContents::INLINE,
            );
            context.device.cmd_bind_descriptor_sets(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                layout.info(),
                0,
                &[descriptor.set()],
                &[],
            );
            context.device.cmd_bind_pipeline(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                pipelines.index(0),
            );

            context.device.cmd_bind_vertex_buffers(
                command_buffer,
                0,
                &[*vertex_buffer.object()],
                &[0],
            );
            context.device.cmd_bind_index_buffer(
                command_buffer,
                *index_buffer.object(),
                0,
                IndexType::UINT32,
            );

            context
                .device
                .cmd_draw_indexed(command_buffer, indices.len() as u32, 1, 0, 0, 1);

            context.device.cmd_end_render_pass(command_buffer);
            context.device.end_command_buffer(command_buffer).unwrap();
        }
    }
    // https://vulkan-tutorial.com/en/Drawing_a_triangle/Drawing/Rendering_and_presentation
    let create_info = SemaphoreCreateInfoBuilder::new();
    let image_available_semaphores: Vec<_> = (0..FRAMES_IN_FLIGHT)
        .map(|_| unsafe { context.device.create_semaphore(&create_info, None, None) }.unwrap())
        .collect();
    let render_finished_semaphores: Vec<_> = (0..FRAMES_IN_FLIGHT)
        .map(|_| unsafe { context.device.create_semaphore(&create_info, None, None) }.unwrap())
        .collect();

    let create_info = FenceCreateInfoBuilder::new().flags(FenceCreateFlags::SIGNALED);
    let in_flight_fences: Vec<_> = (0..FRAMES_IN_FLIGHT)
        .map(|_| unsafe { context.device.create_fence(&create_info, None, None) }.unwrap())
        .collect();
    let mut images_in_flight: Vec<_> = swapchain.images().iter().map(|_| Fence::null()).collect();

    let mut frame = 0;
    event_loop.run(move |event, _, control_flow| match event {
        Event::NewEvents(StartCause::Init) => {
            *control_flow = ControlFlow::Poll;
        }
        Event::WindowEvent { event, .. } => match event {
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            _ => (),
        },
        Event::DeviceEvent { event, .. } => match event {
            DeviceEvent::Key(KeyboardInput {
                virtual_keycode: Some(keycode),
                state,
                ..
            }) => match (keycode, state) {
                (VirtualKeyCode::Escape, ElementState::Released) => {
                    *control_flow = ControlFlow::Exit
                }
                _ => (),
            },
            _ => (),
        },
        Event::MainEventsCleared => {
            unsafe {
                context
                    .device
                    .wait_for_fences(&[in_flight_fences[frame]], true, u64::MAX)
                    .unwrap();
            }

            let image_index = unsafe {
                context.device.acquire_next_image_khr(
                    swapchain.info(),
                    u64::MAX,
                    image_available_semaphores[frame],
                    Fence::null(),
                    None,
                )
            }
            .unwrap();

    
            images_in_flight[image_index as usize] = in_flight_fences[frame];

            let wait_semaphores = vec![image_available_semaphores[frame]];
            let command_buffers = vec![command_buffers[image_index as usize]];
            let signal_semaphores = vec![render_finished_semaphores[frame]];
            let submit_info = SubmitInfoBuilder::new()
                .wait_semaphores(&wait_semaphores)
                .wait_dst_stage_mask(&[PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
                .command_buffers(&command_buffers)
                .signal_semaphores(&signal_semaphores);
            unsafe {
                let in_flight_fence = in_flight_fences[frame];
                context.device.reset_fences(&[in_flight_fence]).unwrap();
                context
                    .device
                    .queue_submit(context.render_queue, &[submit_info], in_flight_fence)
                    .unwrap()
            }

            unsafe {
                context.device.queue_present_khr(
                    context.render_queue,
                    &PresentInfoKHRBuilder::new()
                        .wait_semaphores(&signal_semaphores)
                        .swapchains(&vec![swapchain.info()])
                        .image_indices(&vec![image_index]),
                )
            }
            .unwrap();

            frame = (frame + 1) % FRAMES_IN_FLIGHT;
        }
        Event::LoopDestroyed => unsafe {
            context.wait_idle();
            context.device.destroy_command_pool(command_pool, None);
        },
        _ => (),
    })
}
