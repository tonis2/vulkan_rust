use winit::{
    event::{
        DeviceEvent, ElementState, Event, KeyboardInput, StartCause, VirtualKeyCode, WindowEvent,
    },
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

mod utils;

use std::{ffi::CString, mem, path::Path, sync::Arc};

use utils::{
    gltf_importer::{Importer, MaterialRaw, Vertex},
    Camera, CameraRaw,
};

use vulkan::*;

const FRAMES_IN_FLIGHT: usize = 2;
#[derive(Clone, Debug, Copy)]
pub struct SpecializationData {
    pub materials_amount: u32,
    pub textures_amount: u32,
}

#[repr(C)]
#[derive(Clone, Debug, Copy)]
pub struct Transform {
    pub transform: cgmath::Matrix4<f32>,
}

unsafe impl bytemuck::Zeroable for SpecializationData {}
unsafe impl bytemuck::Pod for SpecializationData {}

unsafe impl bytemuck::Zeroable for Transform {}
unsafe impl bytemuck::Pod for Transform {}

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("test")
        .with_resizable(false)
        .with_inner_size(winit::dpi::LogicalSize::new(800.0, 600.0))
        .build(&event_loop)
        .unwrap();

    let context = Arc::new(Context::new("test", &window));
    let swapchain = Swapchain::new(context.clone());

    let command_pool = context.create_command_pool(
        CommandPoolCreateInfoBuilder::new().queue_family_index(context.graphics_queue_index),
    );

    let mut camera = Camera::new(cgmath::Point3::new(0.0, 0.0, 0.0), 15.0, 1.3);

    let gltf_scene = Importer::load(Path::new("examples/assets/multi_texture.gltf"))
        .build(&context, command_pool);

    let empty_image = Texture::from_file(
        Path::new("examples/assets/wood.jpg"),
        &context,
        command_pool,
    );

    let depth_texture = Texture::create_depth_image(&context);

    let texture_count: u32 = match gltf_scene.textures.len() {
        0 => 1,
        _ => gltf_scene.textures.len() as u32,
    };

    //Create Buffers for data

    let camera_buffer = context.create_buffer(
        BufferCreateInfoBuilder::new()
            .sharing_mode(SharingMode::EXCLUSIVE)
            .usage(BufferUsageFlags::UNIFORM_BUFFER)
            .size(mem::size_of::<CameraRaw>() as DeviceSize),
        bytemuck::cast_slice(&[camera.raw()]),
    );
    let vertex_buffer = gltf_scene.vertex_buffer(&context);
    let index_buffer = gltf_scene.index_buffer(&context);
    let materials_buffer = gltf_scene.materials_buffer(&context);

    //Create bindings

    let input_binding = vec![VertexInputBindingDescriptionBuilder::new()
        .input_rate(VertexInputRate::VERTEX)
        .binding(0)
        .stride(mem::size_of::<Vertex>() as u32)];

    let material_bindings: Vec<DescriptorBufferInfo> = gltf_scene
        .get_materials_data()
        .iter()
        .enumerate()
        .map(|(index, _material)| DescriptorBufferInfo {
            buffer: *materials_buffer.object(),
            offset: index as u64 * context.get_ubo_alignment::<MaterialRaw>() as DeviceSize,
            range: context.get_ubo_alignment::<MaterialRaw>() as DeviceSize,
        })
        .collect();

    // (index * context.get_storage_buffer_alignment::<MaterialRaw>() as usize) as DeviceSize
    let texture_bindings: Vec<DescriptorImageInfo> = {
        if gltf_scene.textures.len() > 0 {
            gltf_scene
                .textures
                .iter()
                .map(|texture| DescriptorImageInfo {
                    sampler: texture.sampler(),
                    image_view: texture.view(),
                    image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                })
                .collect()
        } else {
            vec![DescriptorImageInfo {
                sampler: empty_image.sampler(),
                image_view: empty_image.view(),
                image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            }]
        }
    };

    // Descriptor

    let descriptor = Descriptor::new(
        vec![
            DescriptorEntry {
                bind_index: 0,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::VERTEX,
                buffer_info: Some(vec![DescriptorBufferInfo {
                    buffer: *camera_buffer.object(),
                    offset: 0,
                    range: mem::size_of::<CameraRaw>() as DeviceSize,
                }]),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 1,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::FRAGMENT,
                buffer_info: Some(material_bindings),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 2,
                bind_type: DescriptorType::COMBINED_IMAGE_SAMPLER,
                flag: ShaderStageFlags::FRAGMENT,
                image_info: Some(texture_bindings),
                ..Default::default()
            },
        ],
        context.clone(),
    );

    let spez_data = SpecializationData {
        materials_amount: gltf_scene.materials.len() as u32,
        textures_amount: texture_count,
    };

    let entries = vec![
        SpecializationMapEntryBuilder::new()
            .constant_id(0)
            .offset(offset_of!(SpecializationData, materials_amount) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(1)
            .offset(offset_of!(SpecializationData, textures_amount) as _)
            .size(mem::size_of::<u32>()),
    ];

    let specialization_info = SpecializationInfoBuilder::new()
        .map_entries(&entries)
        .data(bytemuck::bytes_of(&spez_data));
    //Create shader
    let entry_point = CString::new("main").unwrap();

    let vertex_module = Shader::build(
        Path::new("examples/shaders/gltf/gltf.vert.spv"),
        context.clone(),
    );
    let shader_module = Shader::build(
        Path::new("examples/shaders/gltf/gltf.frag.spv"),
        context.clone(),
    );

    let shader_stages = vec![
        PipelineShaderStageCreateInfoBuilder::new()
            .stage(ShaderStageFlagBits::VERTEX)
            .name(&entry_point)
            .module(vertex_module.module()),
        PipelineShaderStageCreateInfoBuilder::new()
            .stage(ShaderStageFlagBits::FRAGMENT)
            .name(&entry_point)
            .module(shader_module.module())
            .specialization_info(&specialization_info),
    ];

    //Pipeline settings
    let input_descriptions = vec![
        VertexInputAttributeDescriptionBuilder::new()
            .location(0)
            .binding(0)
            .format(Format::R32G32B32_SFLOAT)
            .offset(offset_of!(Vertex, position) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(1)
            .binding(0)
            .format(Format::R32G32B32A32_SFLOAT)
            .offset(offset_of!(Vertex, color) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(2)
            .binding(0)
            .format(Format::R32G32B32A32_SFLOAT)
            .offset(offset_of!(Vertex, tangents) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(3)
            .binding(0)
            .format(Format::R32G32B32_SFLOAT)
            .offset(offset_of!(Vertex, normal) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(4)
            .binding(0)
            .format(Format::R32G32_SFLOAT)
            .offset(offset_of!(Vertex, uv) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(5)
            .binding(0)
            .format(Format::R32_SINT)
            .offset(offset_of!(Vertex, material_id) as u32),
    ];

    let vertex_input = PipelineVertexInputStateCreateInfoBuilder::new()
        .vertex_binding_descriptions(&input_binding)
        .vertex_attribute_descriptions(&input_descriptions);

    let input_assembly = PipelineInputAssemblyStateCreateInfoBuilder::new()
        .topology(PrimitiveTopology::TRIANGLE_LIST)
        .primitive_restart_enable(false);

    let viewports = vec![ViewportBuilder::new()
        .x(0.0)
        .y(0.0)
        .width(context.extent().width as f32)
        .height(context.extent().height as f32)
        .min_depth(0.0)
        .max_depth(1.0)];

    let scissors = vec![Rect2DBuilder::new()
        .offset(Offset2D { x: 0, y: 0 })
        .extent(context.extent())];

    let viewport_state = PipelineViewportStateCreateInfoBuilder::new()
        .viewports(&viewports)
        .scissors(&scissors);

    let rasterizer = PipelineRasterizationStateCreateInfoBuilder::new()
        .polygon_mode(PolygonMode::FILL)
        .line_width(1.0)
        .front_face(FrontFace::COUNTER_CLOCKWISE);

    let multisampling = PipelineMultisampleStateCreateInfoBuilder::new()
        .sample_shading_enable(false)
        .rasterization_samples(SampleCountFlagBits::_1);

    let color_blend_attachments = vec![PipelineColorBlendAttachmentStateBuilder::new()
        .color_write_mask(
            ColorComponentFlags::R
                | ColorComponentFlags::G
                | ColorComponentFlags::B
                | ColorComponentFlags::A,
        )];

    let color_blending = PipelineColorBlendStateCreateInfoBuilder::new()
        .logic_op(LogicOp::CLEAR)
        .attachments(&color_blend_attachments);

    let noop_stencil_state = StencilOpState {
        fail_op: StencilOp::KEEP,
        pass_op: StencilOp::KEEP,
        depth_fail_op: StencilOp::KEEP,
        compare_op: CompareOp::ALWAYS,
        ..Default::default()
    };

    let depth_stencil = PipelineDepthStencilStateCreateInfo {
        depth_test_enable: 1,
        depth_write_enable: 1,
        depth_compare_op: CompareOp::LESS_OR_EQUAL,
        front: noop_stencil_state,
        back: noop_stencil_state,
        max_depth_bounds: 1.0,
        ..Default::default()
    };

    let push_constant = PushConstantRangeBuilder::new()
        .stage_flags(ShaderStageFlags::VERTEX)
        .size(mem::size_of::<Transform>() as u32)
        .offset(0);

    let pipeline_layout = Pipeline::layout(
        PipelineLayoutCreateInfoBuilder::new()
            .set_layouts(&[descriptor.layout()])
            .push_constant_ranges(&[push_constant]),
        context.clone(),
    );

    //Renderpass building
    let attachments = vec![
        AttachmentDescriptionBuilder::new()
            .format(context.format())
            .samples(SampleCountFlagBits::_1)
            .load_op(AttachmentLoadOp::CLEAR)
            .store_op(AttachmentStoreOp::STORE)
            .stencil_load_op(AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(AttachmentStoreOp::DONT_CARE)
            .initial_layout(ImageLayout::UNDEFINED)
            .final_layout(ImageLayout::PRESENT_SRC_KHR),
        AttachmentDescriptionBuilder::new()
            .format(depth_texture.format())
            .samples(SampleCountFlagBits::_1)
            .load_op(AttachmentLoadOp::CLEAR)
            .store_op(AttachmentStoreOp::DONT_CARE)
            .stencil_load_op(AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(AttachmentStoreOp::DONT_CARE)
            .initial_layout(ImageLayout::UNDEFINED)
            .final_layout(ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL),
    ];

    let color_attachment_refs = vec![AttachmentReferenceBuilder::new()
        .attachment(0)
        .layout(ImageLayout::COLOR_ATTACHMENT_OPTIMAL)];

    let depth_attachment_ref = AttachmentReferenceBuilder::new()
        .attachment(1)
        .layout(ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    let subpasses = vec![SubpassDescriptionBuilder::new()
        .pipeline_bind_point(PipelineBindPoint::GRAPHICS)
        .color_attachments(&color_attachment_refs)
        .depth_stencil_attachment(&depth_attachment_ref)];
    let dependencies = vec![SubpassDependencyBuilder::new()
        .src_subpass(SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .src_access_mask(AccessFlags::empty())
        .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_READ | AccessFlags::COLOR_ATTACHMENT_WRITE)];

    let renderpass = Renderpass::new(
        *RenderPassCreateInfoBuilder::new()
            .attachments(&attachments)
            .subpasses(&subpasses)
            .dependencies(&dependencies),
        context.clone(),
    );

    //Build pipeline
    let pipeline_info = GraphicsPipelineCreateInfoBuilder::new()
        .stages(&shader_stages)
        .vertex_input_state(&vertex_input)
        .input_assembly_state(&input_assembly)
        .viewport_state(&viewport_state)
        .rasterization_state(&rasterizer)
        .multisample_state(&multisampling)
        .depth_stencil_state(&depth_stencil)
        .color_blend_state(&color_blending)
        .layout(pipeline_layout.info())
        .render_pass(renderpass.info())
        .subpass(0);

    let pipelines = Pipeline::graphics_pipelines(&vec![pipeline_info], context.clone());

    // https://vulkan-tutorial.com/Drawing_a_triangle/Drawing/Framebuffers
    let framebuffers: Vec<Framebuffer> = swapchain
        .images()
        .iter()
        .map(|image_view| {
            Framebuffer::new(
                *FramebufferCreateInfoBuilder::new()
                    .render_pass(renderpass.info())
                    .attachments(&vec![*image_view, depth_texture.view()])
                    .width(context.extent().width as u32)
                    .height(context.extent().height as u32)
                    .layers(1),
                context.clone(),
            )
        })
        .collect();

    let command_buffers = context.create_command_buffers(
        CommandBufferAllocateInfoBuilder::new()
            .command_pool(command_pool)
            .level(CommandBufferLevel::PRIMARY)
            .command_buffer_count(swapchain.image_count()),
    );

    for (index, &command_buffer) in command_buffers.iter().enumerate() {
        let begin_info = CommandBufferBeginInfoBuilder::new();
        unsafe {
            context
                .device
                .begin_command_buffer(command_buffer, &begin_info)
        }
        .unwrap();

        let clear_values = vec![
            ClearValue {
                color: ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            ClearValue {
                // clear value for depth buffer
                depth_stencil: ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];
        let begin_info = RenderPassBeginInfoBuilder::new()
            .render_pass(renderpass.info())
            .framebuffer(framebuffers[index].info())
            .render_area(Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: context.extent(),
            })
            .clear_values(&clear_values);

        unsafe {
            context.device.cmd_begin_render_pass(
                command_buffer,
                &begin_info,
                SubpassContents::INLINE,
            );
            context.device.cmd_bind_pipeline(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                pipelines.index(0),
            );

            context.device.cmd_bind_descriptor_sets(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                pipeline_layout.info(),
                0,
                &[descriptor.set()],
                &[],
            );

            for node in &gltf_scene.nodes {
                if let Some(mesh_index) = node.mesh_index {
                    let mesh = gltf_scene.get_mesh(mesh_index);

                    let transform: Vec<u8> = bytemuck::cast_slice(&[Transform {
                        transform: node.transform_matrix,
                    }])
                    .to_vec();

                    mesh.primitives.iter().for_each(|primitive| {
                        context.device.cmd_bind_vertex_buffers(
                            command_buffer,
                            0,
                            &[*vertex_buffer.object()],
                            &[primitive.vertex_offset as u64],
                        );
                        context.device.cmd_bind_index_buffer(
                            command_buffer,
                            *index_buffer.object(),
                            primitive.indice_offset as u64,
                            IndexType::UINT32,
                        );

                        context.device.cmd_push_constants(
                            command_buffer,
                            pipeline_layout.info(),
                            ShaderStageFlags::VERTEX,
                            0,
                            mem::size_of::<Transform>() as u32,
                            transform.as_ptr() as *const std::ffi::c_void,
                        );
                        context.device.cmd_draw_indexed(
                            command_buffer,
                            primitive.indices_len as u32,
                            1,
                            0,
                            0,
                            0,
                        );
                    });
                }
            }

            context.device.cmd_end_render_pass(command_buffer);
            context.device.end_command_buffer(command_buffer).unwrap();
        }
    }

    //Create user events listener
    let mut events = utils::events::Event::new();

    // https://vulkan-tutorial.com/en/Drawing_a_triangle/Drawing/Rendering_and_presentation
    let semaphore_info = SemaphoreCreateInfoBuilder::new();
    let fence_info = FenceCreateInfoBuilder::new().flags(FenceCreateFlags::SIGNALED);

    let image_available_semaphores =
        SemaphorePresentation::new(0..FRAMES_IN_FLIGHT, &semaphore_info, &context);
    let render_finished_semaphores =
        SemaphorePresentation::new(0..FRAMES_IN_FLIGHT, &semaphore_info, &context);
    let in_flight_fences = FencePresentation::new(0..FRAMES_IN_FLIGHT, &fence_info, &context);

    let mut images_in_flight: Vec<_> = swapchain.images().iter().map(|_| Fence::null()).collect();

    let mut frame = 0;
    event_loop.run(move |event, _, control_flow| match event {
        Event::NewEvents(StartCause::Init) => {
            *control_flow = ControlFlow::Poll;
        }
        Event::WindowEvent { event, .. } => match event {
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            _ => {
                events.handle_event(event);
                if events.event_happened {
                    //Camera updates
                    camera.handle_events(&events);
                    context.update_buffer(&camera_buffer, camera.as_bytes());
                    events.clear();
                }
            }
        },
        Event::DeviceEvent { event, .. } => match event {
            DeviceEvent::Key(KeyboardInput {
                virtual_keycode: Some(keycode),
                state,
                ..
            }) => match (keycode, state) {
                (VirtualKeyCode::Escape, ElementState::Released) => {
                    *control_flow = ControlFlow::Exit
                }
                _ => (),
            },
            _ => (),
        },
        Event::MainEventsCleared => {
            unsafe {
                context
                    .device
                    .wait_for_fences(&[in_flight_fences.get(frame)], true, u64::MAX)
                    .unwrap();
            }

            let image_index = unsafe {
                context.device.acquire_next_image_khr(
                    swapchain.info(),
                    u64::MAX,
                    image_available_semaphores.get(frame),
                    Fence::null(),
                    None,
                )
            }
            .unwrap();

            images_in_flight[image_index as usize] = in_flight_fences.get(frame);

            let wait_semaphores = vec![image_available_semaphores.get(frame)];
            let command_buffers = vec![command_buffers[image_index as usize]];
            let signal_semaphores = vec![render_finished_semaphores.get(frame)];

            let submit_info = SubmitInfoBuilder::new()
                .wait_semaphores(&wait_semaphores)
                .wait_dst_stage_mask(&[PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT])
                .command_buffers(&command_buffers)
                .signal_semaphores(&signal_semaphores);
            unsafe {
                let in_flight_fence = in_flight_fences.get(frame);
                context.device.reset_fences(&[in_flight_fence]).unwrap();
                context
                    .device
                    .queue_submit(context.render_queue, &[submit_info], in_flight_fence)
                    .unwrap()
            }

            unsafe {
                context.device.queue_present_khr(
                    context.render_queue,
                    &PresentInfoKHRBuilder::new()
                        .wait_semaphores(&signal_semaphores)
                        .swapchains(&vec![swapchain.info()])
                        .image_indices(&vec![image_index]),
                )
            }
            .unwrap();

            frame = (frame + 1) % FRAMES_IN_FLIGHT;
        }
        Event::LoopDestroyed => {}
        _ => (),
    })
}
