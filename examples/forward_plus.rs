use winit::{
    event::{
        DeviceEvent, ElementState, Event, KeyboardInput, StartCause, VirtualKeyCode, WindowEvent,
    },
    event_loop::{ControlFlow, EventLoop},
    window::WindowBuilder,
};

mod utils;

use std::{ffi::CString, mem, path::Path, sync::Arc };

use utils::{
    gltf_importer::{Importer, Light, MaterialRaw, Vertex},
    Camera, CameraRaw,
};

use vulkan::*;

// Settings definitions
const FRAMES_IN_FLIGHT: usize = 2;
const MAX_POINT_LIGHT_PER_TILE: u32 = 1023;
const TILE_SIZE: u32 = 16;
const SCREEN_WIDTH: u32 = 800;
const SCREEN_HEIGHT: u32 = 600;

//Shader specialization constants
#[derive(Clone, Debug, Copy)]
pub struct ForwardConstants {
    pub materials_amount: u32,
    pub textures_amount: u32,
    pub lights_amount: u32,
}

#[derive(Clone, Debug, Copy)]
pub struct ComputeConstants {
    pub lights_amount: u32,
    pub max_points_per_light: u32,
    pub tile_size: u32,
    pub screen_width: u32,
    pub screen_height: u32,
    pub row_count: u32,
    pub column_count: u32,
}

#[repr(C)]
#[derive(Clone, Copy)]
pub struct LightVisibility {
    pub count: u32,
    pub indices: [u32; MAX_POINT_LIGHT_PER_TILE as usize],
}

#[repr(C)]
#[derive(Clone, Debug, Copy)]
pub struct TransformPush {
    pub transform: cgmath::Matrix4<f32>,
}

unsafe impl bytemuck::Zeroable for TransformPush {}
unsafe impl bytemuck::Pod for TransformPush {}

unsafe impl bytemuck::Zeroable for ComputeConstants {}
unsafe impl bytemuck::Pod for ComputeConstants {}

unsafe impl bytemuck::Zeroable for LightVisibility {}
unsafe impl bytemuck::Pod for LightVisibility {}

//Settings

fn main() {
    let event_loop = EventLoop::new();
    let window = WindowBuilder::new()
        .with_title("test")
        .with_resizable(false)
        .with_inner_size(winit::dpi::LogicalSize::new(SCREEN_WIDTH, SCREEN_HEIGHT))
        .build(&event_loop)
        .unwrap();

    let context = Arc::new(Context::new("test", &window));
    let swapchain = Swapchain::new(context.clone());

    let render_command_pool = context.create_command_pool(
        CommandPoolCreateInfoBuilder::new().queue_family_index(context.graphics_queue_index),
    );

    let depth_command_pool = context.create_command_pool(
        CommandPoolCreateInfoBuilder::new().queue_family_index(context.graphics_queue_index),
    );

    let compute_command_pool = context.create_command_pool(
        CommandPoolCreateInfoBuilder::new().queue_family_index(context.compute_queue_index),
    );

    let mut camera = Camera::new(cgmath::Point3::new(0.0, 0.0, 0.0), 15.0, 1.3);

    let gltf_scene = Importer::load(Path::new("examples/assets/multi_texture.gltf"))
        .build(&context, render_command_pool);

    let empty_image = Texture::from_file(
        Path::new("examples/assets/wood.jpg"),
        &context,
        render_command_pool,
    );

    let depth_texture = Texture::create_depth_image(&context);

    let texture_count: u32 = match gltf_scene.textures.len() {
        0 => 1,
        _ => gltf_scene.textures.len() as u32,
    };

    //Light culling data

    let row_count: u32 = (context.extent().width as u32 - 1) / TILE_SIZE + 1;
    let column_count: u32 = (context.extent().height as u32 - 1) / TILE_SIZE + 1;
    let culling_data = LightVisibility {
        count: 0,
        indices: [0; MAX_POINT_LIGHT_PER_TILE as usize],
    };

    //Create Buffers for data

    let camera_buffer = context.create_buffer(
        BufferCreateInfoBuilder::new()
            .sharing_mode(SharingMode::EXCLUSIVE)
            .usage(BufferUsageFlags::UNIFORM_BUFFER)
            .size(mem::size_of::<CameraRaw>() as DeviceSize),
        bytemuck::cast_slice(&[camera.raw()]),
    );

    let vertex_buffer = gltf_scene.vertex_buffer(&context);
    let index_buffer = gltf_scene.index_buffer(&context);
    let materials_buffer = gltf_scene.materials_buffer(&context);
    let light_buffer = gltf_scene.lights_buffer(&context);

    let culling_buffer = context.create_buffer(
        BufferCreateInfoBuilder::new()
            .sharing_mode(SharingMode::EXCLUSIVE)
            .usage(BufferUsageFlags::STORAGE_BUFFER)
            .size(
                (row_count
                    * column_count
                    * context.get_storage_buffer_alignment::<LightVisibility>())
                    as DeviceSize,
            ),
        bytemuck::cast_slice(&[culling_data]),
    );

    //Create bindings

    let material_bindings: Vec<DescriptorBufferInfo> = gltf_scene
        .get_materials_data()
        .iter()
        .enumerate()
        .map(|(index, _material)| DescriptorBufferInfo {
            buffer: *materials_buffer.object(),
            offset: index as u64 * context.get_ubo_alignment::<MaterialRaw>() as DeviceSize,
            range: context.get_ubo_alignment::<MaterialRaw>() as DeviceSize,
        })
        .collect();

    let light_bindings: Vec<DescriptorBufferInfo> = gltf_scene
        .lights
        .iter()
        .enumerate()
        .map(|(index, _light)| DescriptorBufferInfo {
            buffer: *light_buffer.object(),
            offset: index as u64 * context.get_ubo_alignment::<Light>() as DeviceSize,
            range: context.get_ubo_alignment::<Light>() as DeviceSize,
        })
        .collect();

    let texture_bindings: Vec<DescriptorImageInfo> = {
        if gltf_scene.textures.len() > 0 {
            gltf_scene
                .textures
                .iter()
                .map(|texture| DescriptorImageInfo {
                    sampler: texture.sampler(),
                    image_view: texture.view(),
                    image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                })
                .collect()
        } else {
            vec![DescriptorImageInfo {
                sampler: empty_image.sampler(),
                image_view: empty_image.view(),
                image_layout: ImageLayout::SHADER_READ_ONLY_OPTIMAL,
            }]
        }
    };

    // Descriptors

    let depth_descriptor = Descriptor::new(
        vec![DescriptorEntry {
            bind_index: 0,
            bind_type: DescriptorType::UNIFORM_BUFFER,
            flag: ShaderStageFlags::VERTEX,
            buffer_info: Some(vec![DescriptorBufferInfo {
                buffer: *camera_buffer.object(),
                offset: 0,
                range: mem::size_of::<CameraRaw>() as DeviceSize,
            }]),
            ..Default::default()
        }],
        context.clone(),
    );

    let forward_descriptor = Descriptor::new(
        vec![
            DescriptorEntry {
                bind_index: 0,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::VERTEX,
                buffer_info: Some(vec![DescriptorBufferInfo {
                    buffer: *camera_buffer.object(),
                    offset: 0,
                    range: mem::size_of::<CameraRaw>() as DeviceSize,
                }]),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 1,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::FRAGMENT,
                buffer_info: Some(material_bindings),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 2,
                bind_type: DescriptorType::COMBINED_IMAGE_SAMPLER,
                flag: ShaderStageFlags::FRAGMENT,
                image_info: Some(texture_bindings),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 3,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::FRAGMENT,
                buffer_info: Some(light_bindings.clone()),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 4,
                bind_type: DescriptorType::STORAGE_BUFFER,
                flag: ShaderStageFlags::FRAGMENT,
                buffer_info: Some(vec![DescriptorBufferInfo {
                    buffer: *culling_buffer.object(),
                    offset: 0,
                    range: context.get_storage_buffer_alignment::<LightVisibility>() as DeviceSize,
                }]),
                ..Default::default()
            },
        ],
        context.clone(),
    );

    let compute_descriptor = Descriptor::new(
        vec![
            DescriptorEntry {
                bind_index: 0,
                bind_type: DescriptorType::STORAGE_BUFFER,
                flag: ShaderStageFlags::COMPUTE,
                buffer_info: Some(vec![DescriptorBufferInfo {
                    buffer: *culling_buffer.object(),
                    offset: 0,
                    range: context.get_storage_buffer_alignment::<LightVisibility>() as DeviceSize,
                }]),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 1,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::COMPUTE,
                buffer_info: Some(vec![DescriptorBufferInfo {
                    buffer: *camera_buffer.object(),
                    offset: 0,
                    range: mem::size_of::<CameraRaw>() as DeviceSize,
                }]),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 2,
                bind_type: DescriptorType::UNIFORM_BUFFER,
                flag: ShaderStageFlags::COMPUTE,
                buffer_info: Some(light_bindings),
                ..Default::default()
            },
            DescriptorEntry {
                bind_index: 3,
                bind_type: DescriptorType::COMBINED_IMAGE_SAMPLER,
                flag: ShaderStageFlags::COMPUTE,
                image_info: Some(vec![DescriptorImageInfo {
                    sampler: depth_texture.sampler(),
                    image_view: depth_texture.view(),
                    image_layout: ImageLayout::DEPTH_STENCIL_READ_ONLY_OPTIMAL,
                }]),
                ..Default::default()
            },
        ],
        context.clone(),
    );

    //Create shaders
    let entry_point = CString::new("main").unwrap();

    //Shader modules
    let depth_prepass_vert = Shader::build(
        Path::new("examples/shaders/forward_plus/depth_prepass.vert.spv"),
        context.clone(),
    );
    let light_culling_comp = Shader::build(
        Path::new("examples/shaders/forward_plus/light_culling.comp.spv"),
        context.clone(),
    );
    let forward_vert_module = Shader::build(
        Path::new("examples/shaders/forward_plus/forward.vert.spv"),
        context.clone(),
    );
    let forward_frag_module = Shader::build(
        Path::new("examples/shaders/forward_plus/forward.frag.spv"),
        context.clone(),
    );

    let forward_specialization_info = ForwardConstants {
        materials_amount: gltf_scene.materials.len() as u32,
        textures_amount: texture_count,
        lights_amount: gltf_scene.lights.len() as u32,
    };

    let forward_spez_entries = vec![
        SpecializationMapEntryBuilder::new()
            .constant_id(0)
            .offset(offset_of!(ForwardConstants, materials_amount) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(1)
            .offset(offset_of!(ForwardConstants, textures_amount) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(2)
            .offset(offset_of!(ForwardConstants, lights_amount) as _)
            .size(mem::size_of::<u32>()),
    ];

    let forward_spez_constant = SpecializationInfoBuilder::new()
        .map_entries(&forward_spez_entries)
        .data(as_byte_slice(&forward_specialization_info));

    let compute_spez_info = ComputeConstants {
        lights_amount: gltf_scene.lights.len() as u32,
        max_points_per_light: MAX_POINT_LIGHT_PER_TILE,
        screen_width: context.extent().width,
        screen_height: context.extent().height,
        row_count,
        column_count,
        tile_size: TILE_SIZE,
    };

    let compute_spez_entries = vec![
        SpecializationMapEntryBuilder::new()
            .constant_id(0)
            .offset(offset_of!(ComputeConstants, lights_amount) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(1)
            .offset(offset_of!(ComputeConstants, max_points_per_light) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(2)
            .offset(offset_of!(ComputeConstants, screen_width) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(3)
            .offset(offset_of!(ComputeConstants, screen_height) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(4)
            .offset(offset_of!(ComputeConstants, row_count) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(5)
            .offset(offset_of!(ComputeConstants, column_count) as _)
            .size(mem::size_of::<u32>()),
        SpecializationMapEntryBuilder::new()
            .constant_id(6)
            .offset(offset_of!(ComputeConstants, tile_size) as _)
            .size(mem::size_of::<u32>()),
    ];

    let compute_spez_constant = SpecializationInfoBuilder::new()
        .map_entries(&compute_spez_entries)
        .data(as_byte_slice(&compute_spez_info));

    let compute_stage = PipelineShaderStageCreateInfoBuilder::new()
        .stage(ShaderStageFlagBits::COMPUTE)
        .name(&entry_point)
        .module(light_culling_comp.module())
        .specialization_info(&compute_spez_constant);

    let depth_stages = vec![PipelineShaderStageCreateInfoBuilder::new()
        .stage(ShaderStageFlagBits::VERTEX)
        .name(&entry_point)
        .module(depth_prepass_vert.module())];

    let forward_stages = vec![
        PipelineShaderStageCreateInfoBuilder::new()
            .stage(ShaderStageFlagBits::VERTEX)
            .name(&entry_point)
            .module(forward_vert_module.module()),
        PipelineShaderStageCreateInfoBuilder::new()
            .stage(ShaderStageFlagBits::FRAGMENT)
            .name(&entry_point)
            .module(forward_frag_module.module())
            .specialization_info(&forward_spez_constant),
    ];

    //Vertex attributes

    let input_binding = vec![VertexInputBindingDescriptionBuilder::new()
        .input_rate(VertexInputRate::VERTEX)
        .binding(0)
        .stride(mem::size_of::<Vertex>() as u32)];

    let forward_attributes = vec![
        VertexInputAttributeDescriptionBuilder::new()
            .location(0)
            .binding(0)
            .format(Format::R32G32B32_SFLOAT)
            .offset(offset_of!(Vertex, position) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(1)
            .binding(0)
            .format(Format::R32G32B32A32_SFLOAT)
            .offset(offset_of!(Vertex, color) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(2)
            .binding(0)
            .format(Format::R32G32B32A32_SFLOAT)
            .offset(offset_of!(Vertex, tangents) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(3)
            .binding(0)
            .format(Format::R32G32B32_SFLOAT)
            .offset(offset_of!(Vertex, normal) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(4)
            .binding(0)
            .format(Format::R32G32_SFLOAT)
            .offset(offset_of!(Vertex, uv) as u32),
        VertexInputAttributeDescriptionBuilder::new()
            .location(5)
            .binding(0)
            .format(Format::R32_SINT)
            .offset(offset_of!(Vertex, material_id) as u32),
    ];

    let depth_attributes = vec![VertexInputAttributeDescriptionBuilder::new()
        .location(0)
        .binding(0)
        .format(Format::R32G32B32_SFLOAT)
        .offset(offset_of!(Vertex, position) as u32)];

    let forward_vertex_input = PipelineVertexInputStateCreateInfoBuilder::new()
        .vertex_binding_descriptions(&input_binding)
        .vertex_attribute_descriptions(&forward_attributes);

    let depth_vertex_input = PipelineVertexInputStateCreateInfoBuilder::new()
        .vertex_binding_descriptions(&input_binding)
        .vertex_attribute_descriptions(&depth_attributes);

    //Renderpasses

    //Depth prepass renderpass

    let depth_pass_attachments = vec![AttachmentDescriptionBuilder::new()
        .format(depth_texture.format())
        .samples(SampleCountFlagBits::_1)
        .load_op(AttachmentLoadOp::CLEAR)
        .store_op(AttachmentStoreOp::STORE)
        .stencil_load_op(AttachmentLoadOp::DONT_CARE)
        .stencil_store_op(AttachmentStoreOp::DONT_CARE)
        .initial_layout(ImageLayout::UNDEFINED)
        .final_layout(ImageLayout::DEPTH_READ_ONLY_STENCIL_ATTACHMENT_OPTIMAL)];

    let depth_attachment_ref = AttachmentReferenceBuilder::new()
        .attachment(0)
        .layout(ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    let depth_subpass = vec![SubpassDescriptionBuilder::new()
        .pipeline_bind_point(PipelineBindPoint::GRAPHICS)
        .depth_stencil_attachment(&depth_attachment_ref)];

    let depth_dependencies = vec![SubpassDependencyBuilder::new()
        .src_subpass(SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(PipelineStageFlags::FRAGMENT_SHADER)
        .src_access_mask(AccessFlags::MEMORY_READ)
        .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_READ | AccessFlags::COLOR_ATTACHMENT_WRITE)];

    let depth_prepass_renderpass = Renderpass::new(
        *RenderPassCreateInfoBuilder::new()
            .attachments(&depth_pass_attachments)
            .subpasses(&depth_subpass)
            .dependencies(&depth_dependencies),
        context.clone(),
    );

    //Depth prepass end

    //Forward renderpass

    let forward_color_attachment_refs = vec![AttachmentReferenceBuilder::new()
        .attachment(0)
        .layout(ImageLayout::COLOR_ATTACHMENT_OPTIMAL)];

    let forward_depth_attachment_ref = AttachmentReferenceBuilder::new()
        .attachment(1)
        .layout(ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL);

    let forward_subpasses = vec![SubpassDescriptionBuilder::new()
        .pipeline_bind_point(PipelineBindPoint::GRAPHICS)
        .color_attachments(&forward_color_attachment_refs)
        .depth_stencil_attachment(&forward_depth_attachment_ref)];

    let forward_dependencies = vec![SubpassDependencyBuilder::new()
        .src_subpass(SUBPASS_EXTERNAL)
        .dst_subpass(0)
        .src_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .src_access_mask(AccessFlags::empty())
        .dst_stage_mask(PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)
        .dst_access_mask(AccessFlags::COLOR_ATTACHMENT_READ | AccessFlags::COLOR_ATTACHMENT_WRITE)];

    let forward_attachments = vec![
        AttachmentDescriptionBuilder::new()
            .format(context.format())
            .samples(SampleCountFlagBits::_1)
            .load_op(AttachmentLoadOp::CLEAR)
            .store_op(AttachmentStoreOp::STORE)
            .stencil_load_op(AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(AttachmentStoreOp::DONT_CARE)
            .initial_layout(ImageLayout::UNDEFINED)
            .final_layout(ImageLayout::PRESENT_SRC_KHR),
        AttachmentDescriptionBuilder::new()
            .format(depth_texture.format())
            .samples(SampleCountFlagBits::_1)
            .load_op(AttachmentLoadOp::CLEAR)
            .store_op(AttachmentStoreOp::DONT_CARE)
            .stencil_load_op(AttachmentLoadOp::DONT_CARE)
            .stencil_store_op(AttachmentStoreOp::DONT_CARE)
            .initial_layout(ImageLayout::UNDEFINED)
            .final_layout(ImageLayout::DEPTH_STENCIL_ATTACHMENT_OPTIMAL),
    ];

    let forward_renderpass = Renderpass::new(
        *RenderPassCreateInfoBuilder::new()
            .attachments(&forward_attachments)
            .subpasses(&forward_subpasses)
            .dependencies(&forward_dependencies),
        context.clone(),
    );

    //Pipeline settings

    let input_assembly = PipelineInputAssemblyStateCreateInfoBuilder::new()
        .topology(PrimitiveTopology::TRIANGLE_LIST)
        .primitive_restart_enable(false);

    let viewports = vec![ViewportBuilder::new()
        .x(0.0)
        .y(0.0)
        .width(context.extent().width as f32)
        .height(context.extent().height as f32)
        .min_depth(0.0)
        .max_depth(1.0)];

    let scissors = vec![Rect2DBuilder::new()
        .offset(Offset2D { x: 0, y: 0 })
        .extent(context.extent())];

    let viewport_state = PipelineViewportStateCreateInfoBuilder::new()
        .viewports(&viewports)
        .scissors(&scissors);

    let rasterizer = PipelineRasterizationStateCreateInfoBuilder::new()
        .polygon_mode(PolygonMode::FILL)
        .line_width(1.0)
        .front_face(FrontFace::COUNTER_CLOCKWISE);

    let multisampling = PipelineMultisampleStateCreateInfoBuilder::new()
        .sample_shading_enable(false)
        .rasterization_samples(SampleCountFlagBits::_1);

    let color_blend_attachments = vec![PipelineColorBlendAttachmentStateBuilder::new()
        .color_write_mask(
            ColorComponentFlags::R
                | ColorComponentFlags::G
                | ColorComponentFlags::B
                | ColorComponentFlags::A,
        )];

    let color_blending = PipelineColorBlendStateCreateInfoBuilder::new()
        .logic_op(LogicOp::CLEAR)
        .attachments(&color_blend_attachments);

    let noop_stencil_state = StencilOpState {
        fail_op: StencilOp::KEEP,
        pass_op: StencilOp::KEEP,
        depth_fail_op: StencilOp::KEEP,
        compare_op: CompareOp::ALWAYS,
        ..Default::default()
    };

    let depth_stencil = PipelineDepthStencilStateCreateInfo {
        depth_test_enable: 1,
        depth_write_enable: 1,
        depth_compare_op: CompareOp::LESS_OR_EQUAL,
        front: noop_stencil_state,
        back: noop_stencil_state,
        max_depth_bounds: 1.0,
        ..Default::default()
    };

    let push_constant = PushConstantRangeBuilder::new()
        .stage_flags(ShaderStageFlags::VERTEX)
        .size(mem::size_of::<TransformPush>() as u32)
        .offset(0);

    //Pipeline layouts
    let forward_layout = Pipeline::layout(
        PipelineLayoutCreateInfoBuilder::new()
            .set_layouts(&[forward_descriptor.layout()])
            .push_constant_ranges(&[push_constant]),
        context.clone(),
    );

    let depth_layout = Pipeline::layout(
        PipelineLayoutCreateInfoBuilder::new()
            .set_layouts(&[depth_descriptor.layout()])
            .push_constant_ranges(&[push_constant]),
        context.clone(),
    );

    let compute_layout = Pipeline::layout(
        PipelineLayoutCreateInfoBuilder::new().set_layouts(&[compute_descriptor.layout()]),
        context.clone(),
    );

    //Build pipelines

    let depth_prepass_pipeline_info = GraphicsPipelineCreateInfoBuilder::new()
        .stages(&depth_stages)
        .vertex_input_state(&depth_vertex_input)
        .input_assembly_state(&input_assembly)
        .viewport_state(&viewport_state)
        .rasterization_state(&rasterizer)
        .multisample_state(&multisampling)
        .depth_stencil_state(&depth_stencil)
        .color_blend_state(&color_blending)
        .layout(depth_layout.info())
        .render_pass(depth_prepass_renderpass.info())
        .subpass(0);

    let forward_pipeline_info = GraphicsPipelineCreateInfoBuilder::new()
        .stages(&forward_stages)
        .vertex_input_state(&forward_vertex_input)
        .input_assembly_state(&input_assembly)
        .viewport_state(&viewport_state)
        .rasterization_state(&rasterizer)
        .multisample_state(&multisampling)
        .depth_stencil_state(&depth_stencil)
        .color_blend_state(&color_blending)
        .layout(forward_layout.info())
        .render_pass(forward_renderpass.info())
        .subpass(0);

    let compute_pipeline_info = ComputePipelineCreateInfoBuilder::new()
        .layout(compute_layout.info())
        .stage(unsafe { compute_stage.discard() });

    let forward_pipelines = Pipeline::graphics_pipelines(
        &vec![depth_prepass_pipeline_info, forward_pipeline_info],
        context.clone(),
    );

    let compute_pipelines =
        Pipeline::compute_pipelines(&vec![compute_pipeline_info], context.clone());

    // Framebuffers

    let depth_framebuffers: Vec<Framebuffer> = swapchain
        .images()
        .iter()
        .map(|image_view| {
            Framebuffer::new(
                *FramebufferCreateInfoBuilder::new()
                    .render_pass(depth_prepass_renderpass.info())
                    .attachments(&vec![depth_texture.view()])
                    .width(context.extent().width as u32)
                    .height(context.extent().height as u32)
                    .layers(1),
                context.clone(),
            )
        })
        .collect();

    let forward_framebuffers: Vec<Framebuffer> = swapchain
        .images()
        .iter()
        .map(|image_view| {
            Framebuffer::new(
                *FramebufferCreateInfoBuilder::new()
                    .render_pass(forward_renderpass.info())
                    .attachments(&vec![*image_view, depth_texture.view()])
                    .width(context.extent().width as u32)
                    .height(context.extent().height as u32)
                    .layers(1),
                context.clone(),
            )
        })
        .collect();

    //Create command buffers

    let depth_command_buffers = context.create_command_buffers(
        CommandBufferAllocateInfoBuilder::new()
            .command_pool(depth_command_pool)
            .level(CommandBufferLevel::PRIMARY)
            .command_buffer_count(swapchain.image_count()),
    );

    let culling_command_buffers = context.create_command_buffers(
        CommandBufferAllocateInfoBuilder::new()
            .command_pool(compute_command_pool)
            .level(CommandBufferLevel::PRIMARY)
            .command_buffer_count(swapchain.image_count()),
    );

    let forward_command_buffers = context.create_command_buffers(
        CommandBufferAllocateInfoBuilder::new()
            .command_pool(render_command_pool)
            .level(CommandBufferLevel::PRIMARY)
            .command_buffer_count(swapchain.image_count()),
    );

    //Build command buffers

    //Render depth prepass
    for (index, &command_buffer) in depth_command_buffers.iter().enumerate() {
        let depth_clear_values = vec![ClearValue {
            // clear value for depth buffer
            depth_stencil: ClearDepthStencilValue {
                depth: 1.0,
                stencil: 0,
            },
        }];
        let begin_info =
            CommandBufferBeginInfoBuilder::new().flags(CommandBufferUsageFlags::SIMULTANEOUS_USE);
        unsafe {
            context
                .device
                .begin_command_buffer(command_buffer, &begin_info)
        }
        .unwrap();

        let depth_begin_info = RenderPassBeginInfoBuilder::new()
            .render_pass(depth_prepass_renderpass.info())
            .framebuffer(depth_framebuffers[index].info())
            .render_area(Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: context.extent(),
            })
            .clear_values(&depth_clear_values);

        unsafe {
            context.device.cmd_begin_render_pass(
                command_buffer,
                &depth_begin_info,
                SubpassContents::INLINE,
            );
            context.device.cmd_bind_pipeline(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                forward_pipelines.index(0),
            );

            context.device.cmd_bind_descriptor_sets(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                depth_layout.info(),
                0,
                &[depth_descriptor.set()],
                &[],
            );

            for node in &gltf_scene.nodes {
                if let Some(mesh_index) = node.mesh_index {
                    let mesh = gltf_scene.get_mesh(mesh_index);

                    let transform: Vec<u8> = bytemuck::cast_slice(&[TransformPush {
                        transform: node.transform_matrix,
                    }])
                    .to_vec();

                    mesh.primitives.iter().for_each(|primitive| {
                        context.device.cmd_bind_vertex_buffers(
                            command_buffer,
                            0,
                            &[*vertex_buffer.object()],
                            &[primitive.vertex_offset as u64],
                        );
                        context.device.cmd_bind_index_buffer(
                            command_buffer,
                            *index_buffer.object(),
                            primitive.indice_offset as u64,
                            IndexType::UINT32,
                        );

                        context.device.cmd_push_constants(
                            command_buffer,
                            forward_layout.info(),
                            ShaderStageFlags::VERTEX,
                            0,
                            mem::size_of::<TransformPush>() as u32,
                            transform.as_ptr() as *const std::ffi::c_void,
                        );
                        context.device.cmd_draw_indexed(
                            command_buffer,
                            primitive.indices_len as u32,
                            1,
                            0,
                            0,
                            0,
                        );
                    });
                }
            }
            context.device.cmd_end_render_pass(command_buffer);
            context.device.end_command_buffer(command_buffer).unwrap();
        }
    }

    //Compute light culling
    for &command_buffer in culling_command_buffers.iter() {
        let begin_info =
            CommandBufferBeginInfoBuilder::new().flags(CommandBufferUsageFlags::SIMULTANEOUS_USE);
        unsafe {
            context
                .device
                .begin_command_buffer(command_buffer, &begin_info)
        }
        .unwrap();

        unsafe {
            context.device.cmd_bind_pipeline(
                command_buffer,
                PipelineBindPoint::COMPUTE,
                compute_pipelines.index(0),
            );
            context.device.cmd_bind_descriptor_sets(
                command_buffer,
                PipelineBindPoint::COMPUTE,
                compute_layout.info(),
                0,
                &[compute_descriptor.set()],
                &[],
            );
            context.device.cmd_dispatch(command_buffer, 1, 1, 1);
            context.device.end_command_buffer(command_buffer).unwrap();
        }
    }

    //Forward final render
    for (index, &command_buffer) in forward_command_buffers.iter().enumerate() {
        let forward_clear_values = vec![
            ClearValue {
                color: ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            ClearValue {
                // clear value for depth buffer
                depth_stencil: ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];
        let begin_info =
            CommandBufferBeginInfoBuilder::new().flags(CommandBufferUsageFlags::SIMULTANEOUS_USE);
        unsafe {
            context
                .device
                .begin_command_buffer(command_buffer, &begin_info)
        }
        .unwrap();

        let forward_begin_info = RenderPassBeginInfoBuilder::new()
            .render_pass(forward_renderpass.info())
            .framebuffer(forward_framebuffers[index].info())
            .render_area(Rect2D {
                offset: Offset2D { x: 0, y: 0 },
                extent: context.extent(),
            })
            .clear_values(&forward_clear_values);

        unsafe {
            //Forward rendering
            context.device.cmd_begin_render_pass(
                command_buffer,
                &forward_begin_info,
                SubpassContents::INLINE,
            );
            context.device.cmd_bind_pipeline(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                forward_pipelines.index(1),
            );

            context.device.cmd_bind_descriptor_sets(
                command_buffer,
                PipelineBindPoint::GRAPHICS,
                forward_layout.info(),
                0,
                &[forward_descriptor.set()],
                &[],
            );

            for node in &gltf_scene.nodes {
                if let Some(mesh_index) = node.mesh_index {
                    let mesh = gltf_scene.get_mesh(mesh_index);
                    let transform: Vec<u8> = bytemuck::cast_slice(&[TransformPush {
                        transform: node.transform_matrix,
                    }])
                    .to_vec();

                    mesh.primitives.iter().for_each(|primitive| {
                        context.device.cmd_bind_vertex_buffers(
                            command_buffer,
                            0,
                            &[*vertex_buffer.object()],
                            &[primitive.vertex_offset as u64],
                        );

                        context.device.cmd_bind_index_buffer(
                            command_buffer,
                            *index_buffer.object(),
                            primitive.indice_offset as u64,
                            IndexType::UINT32,
                        );

                        context.device.cmd_push_constants(
                            command_buffer,
                            forward_layout.info(),
                            ShaderStageFlags::VERTEX,
                            0,
                            mem::size_of::<TransformPush>() as u32,
                            transform.as_ptr() as *const std::ffi::c_void,
                        );

                        context.device.cmd_draw_indexed(
                            command_buffer,
                            primitive.indices_len as u32,
                            1,
                            0,
                            0,
                            0,
                        );
                    });
                }
            }
            context.device.cmd_end_render_pass(command_buffer);
            context.device.end_command_buffer(command_buffer).unwrap();
        }
    }

    //Create user events listener
    let mut events = utils::events::Event::new();

    // https://vulkan-tutorial.com/en/Drawing_a_triangle/Drawing/Rendering_and_presentation
    let semaphore_info = SemaphoreCreateInfoBuilder::new();
    let fence_info = FenceCreateInfoBuilder::new().flags(FenceCreateFlags::SIGNALED);

    let depth_finished_semphamores =
        SemaphorePresentation::new(0..FRAMES_IN_FLIGHT, &semaphore_info, &context);

    let culling_finished_semphamores =
        SemaphorePresentation::new(0..FRAMES_IN_FLIGHT, &semaphore_info, &context);

    let image_available_semaphores =
        SemaphorePresentation::new(0..FRAMES_IN_FLIGHT, &semaphore_info, &context);
    let render_finished_semaphores =
        SemaphorePresentation::new(0..FRAMES_IN_FLIGHT, &semaphore_info, &context);

    let in_flight_fences = FencePresentation::new(0..FRAMES_IN_FLIGHT, &fence_info, &context);

    let mut frame = 0;

    event_loop.run(move |event, _, control_flow| match event {
        Event::NewEvents(StartCause::Init) => {
            *control_flow = ControlFlow::Poll;
        }
        Event::WindowEvent { event, .. } => match event {
            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,
            _ => {
                events.handle_event(event);
                if events.event_happened {
                    //Camera updates
                    camera.handle_events(&events);
                    context.update_buffer(&camera_buffer, camera.as_bytes());
                    events.clear();
                }
            }
        },
        Event::DeviceEvent { event, .. } => match event {
            DeviceEvent::Key(KeyboardInput {
                virtual_keycode: Some(keycode),
                state,
                ..
            }) => match (keycode, state) {
                (VirtualKeyCode::Escape, ElementState::Released) => {
                    *control_flow = ControlFlow::Exit
                }
                _ => (),
            },
            _ => (),
        },
        Event::MainEventsCleared => {
            let image_index = unsafe {
                context.device.acquire_next_image_khr(
                    swapchain.info(),
                    u64::MAX,
                    image_available_semaphores.get(frame),
                    Fence::null(),
                    None,
                )
            }
            .unwrap();

            let depth_finished_signal = vec![depth_finished_semphamores.get(frame)];
            let command_buffers = vec![depth_command_buffers[image_index as usize]];

            //Depth sumbit
            let render_submit_info = SubmitInfoBuilder::new()
                .wait_semaphores(&[])
                .wait_dst_stage_mask(&[])
                .command_buffers(&command_buffers)
                .signal_semaphores(&depth_finished_signal);

            unsafe {
                context
                    .device
                    .queue_submit(context.render_queue, &[render_submit_info], Fence::null())
                    .unwrap();
            }

            //Culling submit
            let culling_finished_semphamore = vec![culling_finished_semphamores.get(frame)];
            let command_buffers = vec![culling_command_buffers[image_index as usize]];

            let light_culling_info = SubmitInfoBuilder::new()
                .wait_semaphores(&depth_finished_signal)
                .wait_dst_stage_mask(&[PipelineStageFlags::VERTEX_SHADER])
                .command_buffers(&command_buffers)
                .signal_semaphores(&culling_finished_semphamore);

            unsafe {
                context
                    .device
                    .queue_submit(
                        context.compute_queue,
                        &vec![light_culling_info],
                        Fence::null(),
                    )
                    .unwrap();
            }

            //Forward submit
            let wait_semaphores = vec![
                image_available_semaphores.get(frame),
                culling_finished_semphamores.get(frame),
            ];
            let command_buffers = vec![forward_command_buffers[image_index as usize]];
            let signal_semaphores = vec![render_finished_semaphores.get(frame)];

            let render_submit_info = SubmitInfoBuilder::new()
                .wait_semaphores(&wait_semaphores)
                .wait_dst_stage_mask(&[
                    PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                    PipelineStageFlags::COMPUTE_SHADER,
                ])
                .command_buffers(&command_buffers)
                .signal_semaphores(&signal_semaphores);

            unsafe {
                let in_flight_fence = in_flight_fences.get(frame);
                context
                    .device
                    .wait_for_fences(&[in_flight_fence], true, std::u64::MAX)
                    .expect("Failed to wait for Fence!");

                context.device.reset_fences(&[in_flight_fence]).unwrap();

                context
                    .device
                    .queue_submit(context.render_queue, &[render_submit_info], in_flight_fence)
                    .unwrap();
            }

            unsafe {
                context.device.queue_present_khr(
                    context.render_queue,
                    &PresentInfoKHRBuilder::new()
                        .wait_semaphores(&signal_semaphores)
                        .swapchains(&vec![swapchain.info()])
                        .image_indices(&vec![image_index]),
                )
            }
            .unwrap();

            frame = (frame + 1) % FRAMES_IN_FLIGHT;
        }
        Event::LoopDestroyed => {}
        _ => (),
    })
}
