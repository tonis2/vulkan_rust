use gltf::{
    khr_lights_punctual::Kind,
    material::{AlphaMode, Material as GltfMaterial, NormalTexture, OcclusionTexture},
    scene::Transform,
};

use std::{path::Path, rc::Rc, sync::Arc};
use vulkan::*;

unsafe impl bytemuck::Zeroable for Vertex {}
unsafe impl bytemuck::Pod for Vertex {}

unsafe impl bytemuck::Zeroable for MaterialRaw {}
unsafe impl bytemuck::Pod for MaterialRaw {}

unsafe impl bytemuck::Zeroable for Light {}
unsafe impl bytemuck::Pod for Light {}

pub struct Importer {
    pub doc: gltf::Document,
    pub buffers: Vec<gltf::buffer::Data>,
    pub images: Vec<gltf::image::Data>,
}

pub struct Scene {
    pub meshes: Vec<Mesh>,
    pub nodes: Vec<Node>,
    pub lights: Vec<Light>,
    pub materials: Vec<Material>,
    pub vertices: Vec<Vertex>,
    pub textures: Rc<Vec<Texture>>,
    pub indices: Vec<u32>,
    pub indices_len: u32,
}

#[repr(C)]
#[derive(Clone, Debug, Copy)]
pub struct Vertex {
    pub position: [f32; 3],
    pub color: [f32; 4],
    pub tangents: [f32; 4],
    pub normal: [f32; 3],
    pub uv: [f32; 2],
    pub material_id: isize,
}

#[derive(Debug, Clone)]
pub struct Node {
    pub index: usize,
    pub mesh_index: Option<usize>,
    pub light_index: Option<usize>,
    pub parent: Option<usize>,
    pub children: Vec<usize>,
    pub translation: Transform,
    pub transform_matrix: cgmath::Matrix4<f32>,
}

#[derive(Clone, Debug)]
pub struct Primitive {
    pub vertex_offset: usize,
    pub indice_offset: u32,
    pub material_id: Option<isize>,
    pub vertice_len: usize,
    pub indices_len: usize,
    pub primitive_topology: PrimitiveTopology,
}

#[allow(dead_code)]
pub struct Mesh {
    pub name: Option<String>,
    pub primitives: Vec<Primitive>,
    pub index: usize,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct Light {
    pub position: [f32; 4],
    pub color: [f32; 3],
    pub intensity: f32,
    pub range: f32,
    pub light_type: u32,
    pub inner_cone_angle: f32,
    pub outer_cone_angle: f32,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct TextureInfo {
    index: i32,
    channel: u32,
}

#[derive(Clone, Debug)]
pub struct Material {
    pub name: Option<String>,
    pub index: Option<usize>,
    pub base_color: [f32; 4],
    pub metallic_factor: f32,
    pub roughness_factor: f32,
    pub emissive_color: [f32; 3],
    pub color: [f32; 4],
    pub emissive: [f32; 3],
    pub occlusion: f32,
    pub color_texture: TextureInfo,
    pub emissive_texture: TextureInfo,
    pub normals_texture: TextureInfo,
    pub occlusion_texture: TextureInfo,
    pub workflow: Workflow,
    pub alpha_mode: u32,
    pub alpha_cutoff: f32,
    pub double_sided: bool,
    pub is_unlit: bool,
}

#[repr(C)]
#[derive(Clone, Debug, Copy)]
pub struct MaterialRaw {
    pub base_color: [f32; 4],
    pub color: [f32; 4],
    pub emissive: [f32; 4],
    pub emissive_color: [f32; 4],

    pub light_settings: [f32; 4],

    pub color_texture: i32,
    pub emissive_texture: i32,
    pub normals_texture: i32,
    pub occlusion_texture: i32,
}

#[derive(Clone, Copy, Debug)]
pub enum Workflow {
    MetallicRoughness(MetallicRoughnessWorkflow),
    SpecularGlossiness(SpecularGlossinessWorkflow),
}

#[derive(Clone, Copy, Debug)]
pub struct MetallicRoughnessWorkflow {
    metallic: f32,
    roughness: f32,
    metallic_roughness_texture: TextureInfo,
}

#[derive(Clone, Copy, Debug)]
pub struct SpecularGlossinessWorkflow {
    specular: [f32; 3],
    glossiness: f32,
    specular_glossiness_texture: TextureInfo,
}

impl Scene {
    pub fn get_mesh(&self, index: usize) -> &Mesh {
        &self.meshes[index]
    }

    pub fn get_materials_data(&self) -> Vec<MaterialRaw> {
        if self.materials.len() > 0 {
            self.materials
                .iter()
                .map(|material| material.raw())
                .collect()
        } else {
            vec![MaterialRaw::default()]
        }
    }

    pub fn vertex_buffer(&self, ctx: &Arc<Context>) -> Allocation<erupt::vk1_0::Buffer> {
        ctx.create_buffer(
            BufferCreateInfoBuilder::new()
                .sharing_mode(SharingMode::EXCLUSIVE)
                .usage(BufferUsageFlags::VERTEX_BUFFER)
                .size((self.vertices.len() * std::mem::size_of::<Vertex>()) as DeviceSize),
            bytemuck::cast_slice(&self.vertices),
        )
    }

    pub fn index_buffer(&self, ctx: &Arc<Context>) -> Allocation<erupt::vk1_0::Buffer> {
        ctx.create_buffer(
            BufferCreateInfoBuilder::new()
                .sharing_mode(SharingMode::EXCLUSIVE)
                .usage(BufferUsageFlags::INDEX_BUFFER)
                .size((self.indices.len() * std::mem::size_of::<u32>()) as DeviceSize),
            bytemuck::cast_slice(&self.indices.clone()),
        )
    }

    pub fn materials_buffer(&self, ctx: &Arc<Context>) -> Allocation<erupt::vk1_0::Buffer> {
        ctx.create_buffer(
            BufferCreateInfoBuilder::new()
                .sharing_mode(SharingMode::EXCLUSIVE)
                .usage(BufferUsageFlags::UNIFORM_BUFFER)
                .size(
                    (self.materials.len() * ctx.get_ubo_alignment::<MaterialRaw>() as usize)
                        as DeviceSize,
                ),
            bytemuck::cast_slice(self.get_materials_data().as_slice()),
        )
    }

    pub fn lights_buffer(&self, ctx: &Arc<Context>) -> Allocation<erupt::vk1_0::Buffer> {
        ctx.create_buffer(
            BufferCreateInfoBuilder::new()
                .sharing_mode(SharingMode::EXCLUSIVE)
                .usage(BufferUsageFlags::UNIFORM_BUFFER)
                .size(
                    (self.lights.len() * ctx.get_ubo_alignment::<MaterialRaw>() as usize)
                        as DeviceSize,
                ),
            bytemuck::cast_slice(self.lights.as_slice()),
        )
    }
}

impl Material {
    pub fn raw(&self) -> MaterialRaw {
        MaterialRaw {
            base_color: self.base_color,
            color: self.color,
            emissive: [self.emissive[0], self.emissive[1], self.emissive[2], 1.0],
            emissive_color: [
                self.emissive_color[0],
                self.emissive_color[1],
                self.emissive_color[2],
                1.0,
            ],
            light_settings: [
                self.metallic_factor,
                self.metallic_factor,
                self.occlusion,
                0.0,
            ],
            color_texture: self.color_texture.index,
            emissive_texture: self.emissive_texture.index,
            normals_texture: self.normals_texture.index,
            occlusion_texture: self.occlusion_texture.index,
        }
    }
}

impl Default for TextureInfo {
    fn default() -> Self {
        TextureInfo {
            index: -1,
            channel: 0,
        }
    }
}

impl Default for Light {
    fn default() -> Self {
        Light {
            position: [0.0, 0.0, 0.0, 0.0],
            color: [1.0, 1.0, 1.0],
            intensity: 0.0,
            range: 0.0,
            light_type: 0,
            inner_cone_angle: 0.0,
            outer_cone_angle: 0.0,
        }
    }
}

impl Default for MaterialRaw {
    fn default() -> Self {
        MaterialRaw {
            base_color: [1.0, 1.0, 1.0, 1.0],
            emissive_color: [1.0, 1.0, 1.0, 1.0],
            color: [1.0, 1.0, 1.0, 1.0],
            emissive: [1.0, 1.0, 1.0, 1.0],
            light_settings: [0.0, 0.0, 0.0, 0.0],
            color_texture: -1,
            emissive_texture: -1,
            normals_texture: -1,
            occlusion_texture: -1,
        }
    }
}

impl<'a> From<GltfMaterial<'a>> for Material {
    fn from(material: GltfMaterial) -> Material {
        let color = match material.pbr_specular_glossiness() {
            Some(pbr) => pbr.diffuse_factor(),
            _ => material.pbr_metallic_roughness().base_color_factor(),
        };

        fn get_texture(texture_info: Option<gltf::texture::Info>) -> TextureInfo {
            texture_info.map_or(
                TextureInfo {
                    index: -1,
                    channel: 0,
                },
                |tex_info| TextureInfo {
                    index: tex_info.texture().index() as i32,
                    channel: tex_info.tex_coord(),
                },
            )
        }

        fn get_normals_texture(texture_info: Option<NormalTexture>) -> TextureInfo {
            texture_info.map_or(
                TextureInfo {
                    index: -1,
                    channel: 0,
                },
                |tex_info| TextureInfo {
                    index: tex_info.texture().index() as i32,
                    channel: tex_info.tex_coord(),
                },
            )
        }

        fn get_occlusion(texture_info: Option<OcclusionTexture>) -> (f32, TextureInfo) {
            let strength = texture_info
                .as_ref()
                .map_or(0.0, |tex_info| tex_info.strength());

            let texture = texture_info.map_or(
                TextureInfo {
                    index: -1,
                    channel: 0,
                },
                |tex_info| TextureInfo {
                    index: tex_info.texture().index() as i32,
                    channel: tex_info.tex_coord(),
                },
            );

            (strength, texture)
        }

        let emissive = material.emissive_factor();

        let color_texture = match material.pbr_specular_glossiness() {
            Some(pbr) => pbr.diffuse_texture(),
            _ => material.pbr_metallic_roughness().base_color_texture(),
        };
        let color_texture = get_texture(color_texture);
        let emissive_texture = get_texture(material.emissive_texture());
        let normals_texture = get_normals_texture(material.normal_texture());
        let (occlusion, occlusion_texture) = get_occlusion(material.occlusion_texture());

        let workflow = match material.pbr_specular_glossiness() {
            Some(pbr) => Workflow::SpecularGlossiness(SpecularGlossinessWorkflow {
                specular: pbr.specular_factor(),
                glossiness: pbr.glossiness_factor(),
                specular_glossiness_texture: get_texture(pbr.specular_glossiness_texture()),
            }),
            _ => {
                let pbr = material.pbr_metallic_roughness();
                Workflow::MetallicRoughness(MetallicRoughnessWorkflow {
                    metallic: pbr.metallic_factor(),
                    roughness: pbr.roughness_factor(),
                    metallic_roughness_texture: get_texture(pbr.metallic_roughness_texture()),
                })
            }
        };

        let alpha_mode = match material.alpha_mode() {
            AlphaMode::Opaque => 1,
            AlphaMode::Mask => 2,
            AlphaMode::Blend => 3,
        };

        Material {
            index: material.index(),
            name: material.name().map(String::from),

            base_color: material.pbr_metallic_roughness().base_color_factor(),
            metallic_factor: material.pbr_metallic_roughness().metallic_factor(),
            roughness_factor: material.pbr_metallic_roughness().roughness_factor(),
            emissive_color: material.emissive_factor(),

            color,
            emissive,
            occlusion,
            color_texture,
            emissive_texture,
            normals_texture,
            occlusion_texture,
            workflow,
            alpha_mode,
            alpha_cutoff: material.alpha_cutoff(),
            double_sided: material.double_sided(),
            is_unlit: material.unlit(),
        }
    }
}

impl Importer {
    //Load gltf data from file
    pub fn load<P: AsRef<Path>>(path: P) -> Importer {
        let (doc, buffers, images) = gltf::import(path).expect("Failed to load gltf file");
        Importer {
            doc,
            buffers,
            images,
        }
    }
    //Parse and build gltf content

    pub fn build(&self, ctx: &Arc<Context>, pool: CommandPool) -> Scene {
        let mut meshes: Vec<Mesh> = Vec::new();
        let mut nodes = Vec::new();
        let mut vertices: Vec<Vertex> = Vec::new();
        let mut indices: Vec<u32> = Vec::new();

        let samplers: Vec<SamplerCreateInfoBuilder> = self
            .doc
            .samplers()
            .map(|sampler| build_sampler(&sampler))
            .collect();

        let textures = self
            .doc
            .textures()
            .map(|texture| {
                let image_index = texture.source().index();
                let sampler_index = texture.sampler().index();
                let image_properties = &self.images[image_index as usize];
                let image_data = Self::get_texture_data(image_properties);
                let mut texture = Texture::from_data(
                    image_data,
                    image_properties.width,
                    image_properties.height,
                    ctx,
                    pool,
                );
                if sampler_index.is_some() {
                    texture.attach_sampler(*samplers[sampler_index.unwrap()])
                } else {
                    texture.attach_sampler(
                        *SamplerCreateInfoBuilder::new()
                            .mag_filter(Filter::LINEAR)
                            .min_filter(Filter::LINEAR)
                            .mipmap_mode(SamplerMipmapMode::LINEAR)
                            .address_mode_u(SamplerAddressMode::REPEAT)
                            .address_mode_v(SamplerAddressMode::REPEAT)
                            .address_mode_w(SamplerAddressMode::CLAMP_TO_EDGE)
                            .max_lod(1.0)
                            .mip_lod_bias(0.0),
                    )
                }
                texture
            })
            .collect();

        let mut lights: Vec<Light> = self.doc.lights().map_or(vec![], |lights| {
            lights
                .map(|light_data| {
                    let mut light = Light {
                        position: [0.0, 0.0, 0.0, 0.0],
                        color: light_data.color(),
                        intensity: light_data.intensity(),
                        range: light_data.range().unwrap_or(0.0),
                        light_type: 0,
                        inner_cone_angle: 0.0,
                        outer_cone_angle: 0.0,
                    };

                    match light_data.kind() {
                        Kind::Directional => light.light_type = 0,
                        Kind::Point => light.light_type = 1,
                        Kind::Spot {
                            inner_cone_angle,
                            outer_cone_angle,
                        } => {
                            light.inner_cone_angle = inner_cone_angle;
                            light.outer_cone_angle = outer_cone_angle;
                            light.light_type = 2;
                        }
                    };
                    light
                })
                .collect()
        });

        let materials: Vec<Material> = self
            .doc
            .materials()
            .map(|material| Material::from(material))
            .collect();

        //Store Nodes
        for node in self.doc.nodes() {
            let children_indices = node
                .children()
                .map(|child| child.index())
                .collect::<Vec<usize>>();

            let local_transform = node.transform();
            let transform_matrix = compute_transform_matrix(&local_transform);
            let mut parent = None;

            //If we encounter ourselves (node) when searching children, we've found our parent
            for potential_parent in self.doc.nodes() {
                if potential_parent
                    .children()
                    .find(|child| child.index() == node.index())
                    .is_some()
                {
                    parent = Some(potential_parent.index());
                }
            }

            //Add transform to light
            if let Some(light) = node.light() {
                lights[light.index()].position = [
                    transform_matrix.w.x,
                    transform_matrix.w.y,
                    transform_matrix.w.z,
                    0.0,
                ];
            }

            nodes.push(Node {
                index: node.index(),
                mesh_index: node.mesh().map(|mesh| mesh.index()),
                light_index: node.light().map(|light| light.index()),
                parent: parent,
                children: children_indices,
                translation: local_transform,
                transform_matrix,
            });
        }

        for mesh in self.doc.meshes() {
            let primitives: Vec<Primitive> = mesh
                .primitives()
                .map(|primitive| {
                    let reader = primitive.reader(|buffer| Some(&self.buffers[buffer.index()]));
                    //Read mesh data
                    use gltf::mesh::Mode;
                    let primitive_topology = match primitive.mode() {
                        Mode::Triangles => PrimitiveTopology::TRIANGLE_LIST,
                        Mode::TriangleStrip => PrimitiveTopology::TRIANGLE_STRIP,
                        Mode::Lines => PrimitiveTopology::LINE_LIST,
                        Mode::LineStrip => PrimitiveTopology::LINE_STRIP,
                        Mode::Points => PrimitiveTopology::POINT_LIST,
                        mode @ _ => panic!("unsupported primitive mode: {:?}", mode),
                    };

                    let indices_data: Option<Vec<u32>> = reader
                        .read_indices()
                        .map(|read_indices| read_indices.into_u32().collect());
                    let colors: Vec<[f32; 4]> = reader
                        .read_colors(0)
                        .map_or(vec![], |color| color.into_rgba_f32().collect());
                    let normals: Vec<[f32; 3]> = reader
                        .read_normals()
                        .map_or(vec![], |normals| normals.collect());
                    let uvs: Vec<[f32; 2]> = reader
                        .read_tex_coords(0)
                        .map_or(vec![], |uvs| uvs.into_f32().collect());
                    let tangents: Vec<[f32; 4]> = reader
                        .read_tangents()
                        .map_or(vec![], |tangents| tangents.collect());
                    let material_id: Option<isize> =
                        primitive.material().index().map(|num| num as isize);
                    let vertices_data: Vec<Vertex> = reader
                        .read_positions()
                        .unwrap()
                        .enumerate()
                        .map(|(index, position)| Vertex {
                            position,
                            color: *colors.get(index).unwrap_or(&[1.0, 1.0, 1.0, 1.0]),
                            uv: *uvs.get(index).unwrap_or(&[0.0, 0.0]),
                            normal: *normals.get(index).unwrap_or(&[1.0, 1.0, 1.0]),
                            material_id: material_id.unwrap_or(-1),
                            tangents: *tangents.get(index).unwrap_or(&[0.0, 0.0, 0.0, 0.0]),
                        })
                        .collect();

                    vertices.extend_from_slice(&vertices_data);

                    let mut indice_offset: u32 = 0;
                    let mut indices_len: usize = 0;
                    if indices_data.is_some() {
                        let current_indices = &indices_data.unwrap();
                        indices_len = current_indices.len();
                        indice_offset = (indices.len() * std::mem::size_of::<u32>()) as u32;
                        indices.extend_from_slice(current_indices);
                    }

                    Primitive {
                        vertex_offset: (vertices.len() - vertices_data.len())
                            * std::mem::size_of::<Vertex>(),
                        indice_offset,
                        indices_len,
                        material_id,
                        vertice_len: vertices_data.len(),
                        primitive_topology,
                    }
                })
                .collect();

            meshes.push(Mesh {
                index: mesh.index(),
                name: mesh.name().map(String::from),
                primitives,
            });
        }

        Scene {
            meshes,
            nodes,
            materials,
            textures: Rc::new(textures),
            lights,
            indices_len: indices.len() as u32,
            vertices,
            indices,
        }
    }

    pub fn get_texture_data(properties: &gltf::image::Data) -> Vec<u8> {
        use image::{Bgr, Bgra, ConvertBuffer, ImageBuffer, Rgb, Rgba};
        type RgbaImage = ImageBuffer<Rgba<u8>, Vec<u8>>;
        type BgraImage = ImageBuffer<Bgra<u8>, Vec<u8>>;
        type RgbImage = ImageBuffer<Rgb<u8>, Vec<u8>>;
        type BgrImage = ImageBuffer<Bgr<u8>, Vec<u8>>;
        match properties.format {
            gltf::image::Format::R8 | gltf::image::Format::R8G8 | gltf::image::Format::R8G8B8 => {
                let rgba: RgbaImage = RgbImage::from_raw(
                    properties.width,
                    properties.height,
                    properties.pixels.clone(),
                )
                .unwrap()
                .convert();

                rgba.into_raw()
            }
            gltf::image::Format::B8G8R8 => {
                let bgra: RgbaImage = BgrImage::from_raw(
                    properties.width,
                    properties.height,
                    properties.pixels.clone(),
                )
                .unwrap()
                .convert();

                bgra.into_raw()
            }
            gltf::image::Format::B8G8R8A8 => {
                let bgra: RgbaImage = BgraImage::from_raw(
                    properties.width,
                    properties.height,
                    properties.pixels.clone(),
                )
                .unwrap()
                .convert();

                bgra.into_raw()
            }
            gltf::image::Format::R8G8B8A8 => properties.pixels.clone(),
            _ => {
                panic!("Unsupported texture format: {:?}", properties.format);
            }
        }
    }
}

fn compute_transform_matrix(transform: &Transform) -> cgmath::Matrix4<f32> {
    match transform {
        Transform::Matrix { matrix } => cgmath::Matrix4::from(*matrix),
        Transform::Decomposed {
            translation,
            rotation: [xr, yr, zr, wr],
            scale: [xs, ys, zs],
        } => {
            let translation =
                cgmath::Matrix4::from_translation(cgmath::Vector3::from(*translation));
            let rotation = cgmath::Matrix4::from(cgmath::Quaternion::new(*wr, *xr, *yr, *zr));
            let scale = cgmath::Matrix4::from_nonuniform_scale(*xs, *ys, *zs);
            translation * rotation * scale
        }
    }
}

fn build_sampler<'a>(sampler: &gltf::texture::Sampler) -> SamplerCreateInfoBuilder<'a> {
    use gltf::texture::MagFilter;
    use gltf::texture::MinFilter;
    use gltf::texture::WrappingMode;

    fn address_mode(wrap_mode: WrappingMode) -> SamplerAddressMode {
        match wrap_mode {
            WrappingMode::ClampToEdge => SamplerAddressMode::CLAMP_TO_EDGE,
            WrappingMode::Repeat => SamplerAddressMode::REPEAT,
            WrappingMode::MirroredRepeat => SamplerAddressMode::MIRRORED_REPEAT,
        }
    };

    fn min_filter_mimap_filter(min_filter: MinFilter) -> (Filter, SamplerMipmapMode) {
        match min_filter {
            MinFilter::Linear => (Filter::LINEAR, SamplerMipmapMode::LINEAR),
            MinFilter::Nearest => (Filter::NEAREST, SamplerMipmapMode::NEAREST),
            MinFilter::LinearMipmapLinear => (Filter::LINEAR, SamplerMipmapMode::LINEAR),
            MinFilter::LinearMipmapNearest => (Filter::LINEAR, SamplerMipmapMode::NEAREST),
            MinFilter::NearestMipmapNearest => (Filter::NEAREST, SamplerMipmapMode::NEAREST),
            MinFilter::NearestMipmapLinear => (Filter::NEAREST, SamplerMipmapMode::LINEAR),
        }
    }

    let (min_filter, mipmap_filter) = min_filter_mimap_filter(
        sampler
            .min_filter()
            .unwrap_or(gltf::texture::MinFilter::Nearest),
    );

    let mag_filter = match sampler
        .mag_filter()
        .unwrap_or(gltf::texture::MagFilter::Nearest)
    {
        MagFilter::Nearest => Filter::NEAREST,
        MagFilter::Linear => Filter::LINEAR,
    };

    SamplerCreateInfoBuilder::new()
        .mag_filter(mag_filter)
        .min_filter(min_filter)
        .mipmap_mode(mipmap_filter)
        .address_mode_u(address_mode(sampler.wrap_s()))
        .address_mode_v(address_mode(sampler.wrap_t()))
        .address_mode_w(SamplerAddressMode::CLAMP_TO_EDGE)
        .max_lod(1.0)
        .mip_lod_bias(0.0)
}
