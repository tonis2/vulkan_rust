use crate::{Context, Texture};
use std::sync::Arc;
use vulkan::*;

pub fn create_empty_texture(
    format: Format,
    width: u32,
    height: u32,
    ctx: &Arc<Context>,
    pool: CommandPool,
) -> Texture {
    let image_info = ImageCreateInfoBuilder::new()
        .image_type(ImageType::_2D)
        .extent(Extent3D {
            width: width,
            height: height,
            depth: 1,
        })
        .usage(ImageUsageFlags::COLOR_ATTACHMENT | ImageUsageFlags::SAMPLED)
        .format(format)
        .mip_levels(1)
        .array_layers(1)
        .samples(SampleCountFlagBits::_1)
        .sharing_mode(SharingMode::EXCLUSIVE)
        .tiling(ImageTiling::OPTIMAL);

    let mut empty_texture = Texture::new(*image_info, ctx.clone());

    let view_info = ImageViewCreateInfoBuilder::new()
        .view_type(ImageViewType::_2D)
        .format(format)
        .image(empty_texture.image())
        .components(ComponentMapping {
            r: ComponentSwizzle::IDENTITY,
            g: ComponentSwizzle::IDENTITY,
            b: ComponentSwizzle::IDENTITY,
            a: ComponentSwizzle::IDENTITY,
        })
        .subresource_range(ImageSubresourceRange {
            aspect_mask: ImageAspectFlags::COLOR,
            base_mip_level: 0,
            level_count: 1,
            base_array_layer: 0,
            layer_count: 1,
        });
    let sampler_info = SamplerCreateInfoBuilder::new()
        .mag_filter(Filter::LINEAR)
        .min_filter(Filter::LINEAR)
        .mipmap_mode(SamplerMipmapMode::LINEAR)
        .address_mode_u(SamplerAddressMode::REPEAT)
        .address_mode_v(SamplerAddressMode::REPEAT)
        .address_mode_w(SamplerAddressMode::REPEAT)
        .max_lod(1.0)
        .mip_lod_bias(0.0)
        .anisotropy_enable(false)
        .max_anisotropy(16.0);
        
    empty_texture.attach_view(*view_info);
    empty_texture.attach_sampler(*sampler_info);


    ctx.apply_pipeline_barrier(
        PipelineStageFlags::BOTTOM_OF_PIPE,
        PipelineStageFlags::FRAGMENT_SHADER,
        ImageMemoryBarrierBuilder::new()
            .src_access_mask(AccessFlags::empty())
            .dst_access_mask(AccessFlags::SHADER_READ)
            .old_layout(ImageLayout::UNDEFINED)
            .new_layout(ImageLayout::SHADER_READ_ONLY_OPTIMAL)
            .image(empty_texture.image())
            .subresource_range(ImageSubresourceRange {
                aspect_mask: ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            }),
        pool,
    );

    empty_texture
}
