mod camera;
pub mod events;
pub mod gltf_importer;
pub use events::Event;
pub mod helpers;
pub use camera::{Camera, CameraRaw};

pub const OPENGL_TO_VULKAN_MATRIX: cgmath::Matrix4<f32> = cgmath::Matrix4::new(
    1.0, 0.0, 0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 0.0, 0.5, 1.0,
);
