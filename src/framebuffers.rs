use super::context::Context;
use erupt::vk1_0::*;
use std::sync::Arc;

pub struct Framebuffer {
    buffer: erupt::vk1_0::Framebuffer,
    ctx: Arc<Context>,
}

impl Framebuffer {
    pub fn info(&self) -> erupt::vk1_0::Framebuffer {
        self.buffer
    }

    pub fn new(info: FramebufferCreateInfo, ctx: Arc<Context>) -> Framebuffer {
        let buffer = unsafe {
            ctx.device
                .create_framebuffer(&info, None, None)
                .expect("Failed to create Framebuffer!")
        };

        Framebuffer {
            buffer,
            ctx: ctx.clone(),
        }
    }
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe {
            self.ctx.device.device_wait_idle().unwrap();
            self.ctx.device.destroy_framebuffer(self.buffer, None);
        }
    }
}
