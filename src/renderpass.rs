use super::context::Context;
use erupt::vk1_0::*;
use std::sync::Arc;
pub struct Renderpass {
    pass: erupt::vk1_0::RenderPass,
    ctx: Arc<Context>,
}

impl Renderpass {
    pub fn new(info: RenderPassCreateInfo, ctx: Arc<Context>) -> Self {
        let pass = unsafe {
            ctx.device
                .create_render_pass(&info, None, None)
                .expect("Failed to create render pass!")
        };
        Self {
            pass,
            ctx: ctx.clone(),
        }
    }

    pub fn info(&self) -> erupt::vk1_0::RenderPass {
        self.pass
    }
}

impl Drop for Renderpass {
    fn drop(&mut self) {
        unsafe { 
            self.ctx.wait_idle();
            self.ctx.device.destroy_render_pass(self.pass, None);
        }
    }
}