use super::context::Context;
use erupt::vk1_0::*;
use std::sync::Arc;
pub struct Pipeline {
    pipe: Vec<erupt::vk1_0::Pipeline>,
    ctx: Arc<Context>,
}

pub struct PipelineLayout {
    layout: erupt::vk1_0::PipelineLayout,
    ctx: Arc<Context>,
}

impl PipelineLayout {
    pub fn info(&self) -> erupt::vk1_0::PipelineLayout {
        self.layout
    }
}

impl Pipeline {
    pub fn graphics_pipelines(
        info: &Vec<GraphicsPipelineCreateInfoBuilder>,
        ctx: Arc<Context>,
    ) -> Self {
        let pipe = unsafe {
            ctx.device
                .create_graphics_pipelines(PipelineCache::null(), info, None)
        }
        .unwrap();

        Self {
            pipe,
            ctx: ctx.clone(),
        }
    }

    pub fn index(&self, index: usize) -> erupt::vk1_0::Pipeline {
        self.pipe[index]
    }

    pub fn compute_pipelines(
        info: &Vec<ComputePipelineCreateInfoBuilder>,
        ctx: Arc<Context>,
    ) -> Self {
        let pipe = unsafe {
            ctx.device
                .create_compute_pipelines(PipelineCache::null(), info, None)
        }
        .unwrap();

        Self {
            pipe,
            ctx: ctx.clone(),
        }
    }

    pub fn layout(info: PipelineLayoutCreateInfoBuilder, ctx: Arc<Context>) -> PipelineLayout {
        let layout = unsafe { ctx.device.create_pipeline_layout(&info, None, None) }.unwrap();
        PipelineLayout {
            layout,
            ctx: ctx.clone(),
        }
    }
}

impl Drop for Pipeline {
    fn drop(&mut self) {
        unsafe {
            self.ctx.wait_idle();
            for pipeline in &self.pipe {
                self.ctx.device.destroy_pipeline(*pipeline, None);
            }
        }
    }
}

impl Drop for PipelineLayout {
    fn drop(&mut self) {
        unsafe {
            self.ctx.wait_idle();
            self.ctx.device.destroy_pipeline_layout(self.layout, None);
        }
    }
}
