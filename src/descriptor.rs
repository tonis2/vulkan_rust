use super::context::Context;
use erupt::vk1_0::*;
use std::ptr;
use std::sync::Arc;

pub struct Descriptor {
    set: erupt::vk1_0::DescriptorSet,
    layout: DescriptorSetLayout,
    pool: DescriptorPool,
    ctx: Arc<Context>,
}
#[derive(Debug, Clone)]
pub struct DescriptorEntry {
    pub flag: ShaderStageFlags,
    pub bind_type: DescriptorType,
    pub bind_index: u32,
    pub buffer_info: Option<Vec<DescriptorBufferInfo>>,
    pub image_info: Option<Vec<DescriptorImageInfo>>,
    pub array_element: u32,
}

impl Default for DescriptorEntry {
    fn default() -> Self {
        Self {
            flag: ShaderStageFlags::default(),
            bind_type: DescriptorType::default(),
            bind_index: 0,
            buffer_info: None,
            image_info: None,
            array_element: 0,
        }
    }
}

impl Descriptor {
    //Creates new pipeline descriptor
    pub fn new(sets: Vec<DescriptorEntry>, ctx: Arc<Context>) -> Self {
        let pool_sizes: &Vec<DescriptorPoolSize> = &sets
            .iter()
            .map(|set| {
                let mut descriptor_count = 0;
                if set.buffer_info.is_some() {
                    let data = set.buffer_info.as_ref().unwrap();
                    descriptor_count = data.len() as u32;
                }

                if set.image_info.is_some() {
                    let data = set.image_info.as_ref().unwrap();
                    descriptor_count = data.len() as u32;
                }

                let pool_description = DescriptorPoolSize {
                    _type: set.bind_type,
                    descriptor_count,
                };

                pool_description
            })
            .collect();

        let pool = unsafe {
            ctx.device
                .create_descriptor_pool(
                    &DescriptorPoolCreateInfo {
                        s_type: StructureType::DESCRIPTOR_POOL_CREATE_INFO,
                        p_next: ptr::null(),
                        flags: DescriptorPoolCreateFlags::empty(),
                        max_sets: ctx.image_count,
                        pool_size_count: pool_sizes.len() as u32,
                        p_pool_sizes: pool_sizes.as_ptr(),
                    },
                    None,
                    None,
                )
                .expect("Failed to create Descriptor Pool!")
        };

        let bindings: Vec<DescriptorSetLayoutBindingBuilder> = sets
            .iter()
            .map(|set| {
                let mut descriptor_count = 0;
                if set.buffer_info.is_some() {
                    let data = set.buffer_info.as_ref().unwrap();
                    descriptor_count = data.len() as u32;
                }

                if set.image_info.is_some() {
                    let data = set.image_info.as_ref().unwrap();
                    descriptor_count = data.len() as u32;
                }

                DescriptorSetLayoutBindingBuilder::new()
                    .binding(set.bind_index)
                    .descriptor_count(descriptor_count)
                    .descriptor_type(set.bind_type)
                    .stage_flags(set.flag)
            })
            .collect();

        let layouts = unsafe {
            vec![ctx
                .device
                .create_descriptor_set_layout(
                    &DescriptorSetLayoutCreateInfoBuilder::new().bindings(&bindings),
                    None,
                    None,
                )
                .expect("Failed to create Descriptor Set Layout!")]
        };

        let desc_info = DescriptorSetAllocateInfoBuilder::new()
            .descriptor_pool(pool)
            .set_layouts(&layouts);
        let desc_set = unsafe { ctx.device.allocate_descriptor_sets(&desc_info) }
            .expect("failed to descriptor set allocate");

        let write_sets: Vec<WriteDescriptorSetBuilder> = sets
            .iter()
            .map(|set| {
                let mut descriptor = WriteDescriptorSetBuilder::new()
                    .dst_set(desc_set[0])
                    .dst_binding(set.bind_index)
                    .descriptor_type(set.bind_type)
                    .dst_array_element(set.array_element);

                if set.buffer_info.is_some() {
                    let data = set.buffer_info.as_ref().unwrap();
                    descriptor.descriptor_count = data.len() as u32;
                    descriptor.p_buffer_info = data.as_ptr();
                }

                if set.image_info.is_some() {
                    let data = set.image_info.as_ref().unwrap();
                    descriptor.descriptor_count = data.len() as u32;
                    descriptor.p_image_info = data.as_ptr();
                }

                descriptor
            })
            .collect();

        unsafe {
            ctx.device.update_descriptor_sets(&write_sets, &[]);
        }

        Self {
            set: desc_set[0],
            layout: layouts[0],
            pool,
            ctx,
        }
    }

    pub fn layout(&self) -> DescriptorSetLayout {
        self.layout
    }

    pub fn set(&self) -> erupt::vk1_0::DescriptorSet {
        self.set
    }
}

impl Drop for Descriptor {
    fn drop(&mut self) {
        unsafe {
            self.ctx.wait_idle();
            self.ctx
                .device
                .destroy_descriptor_set_layout(self.layout, None);
            self.ctx.device.destroy_descriptor_pool(self.pool, None);
        }
    }
}
