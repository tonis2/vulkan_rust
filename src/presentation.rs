use super::context::Context;
use erupt::vk1_0::*;
use std::ops::Range;
use std::sync::Arc;

pub struct FencePresentation {
    entries: Vec<Fence>,
    ctx: Arc<Context>,
}

pub struct SemaphorePresentation {
    entries: Vec<Semaphore>,
    ctx: Arc<Context>,
}

impl FencePresentation {
    pub fn new(
        range: Range<usize>,
        create_info: &FenceCreateInfoBuilder,
        ctx: &Arc<Context>,
    ) -> Self {
        let entries = range
            .map(|_| unsafe { ctx.device.create_fence(&create_info, None, None) }.unwrap())
            .collect();

        Self {
            entries,
            ctx: ctx.clone(),
        }
    }

    pub fn get(&self, index: usize) -> Fence {
        self.entries[index]
    }
}

impl SemaphorePresentation {
    pub fn new(
        range: Range<usize>,
        create_info: &SemaphoreCreateInfo,
        ctx: &Arc<Context>,
    ) -> Self {
        let entries = range
            .map(|_| unsafe { ctx.device.create_semaphore(&create_info, None, None) }.unwrap())
            .collect();

        Self {
            entries,
            ctx: ctx.clone(),
        }
    }

    pub fn get(&self, index: usize) -> Semaphore {
        self.entries[index]
    }
}

impl Drop for FencePresentation {
    fn drop(&mut self) {
        self.ctx.wait_idle();
        for &fence in &self.entries {
            unsafe {
                self.ctx.device.destroy_fence(fence, None);
            }
        }
    }
}

impl Drop for SemaphorePresentation {
    fn drop(&mut self) {
        self.ctx.wait_idle();
        for &fence in &self.entries {
            unsafe {
                self.ctx.device.destroy_semaphore(fence, None);
            }
        }
    }
}
