mod context;
mod descriptor;
mod framebuffers;
mod pipeline;
mod renderpass;
mod shader;
mod swapchain;
mod texture;
mod presentation;

pub use context::Context;
pub use erupt::vk1_0::*;
pub use erupt::extensions::khr_swapchain::*;
pub use erupt::utils::allocator::{MemoryTypeFinder, Allocation};
pub use framebuffers::Framebuffer;
pub use pipeline::Pipeline;
pub use renderpass::Renderpass;
pub use shader::Shader;
pub use swapchain::Swapchain;
pub use descriptor::{Descriptor, DescriptorEntry};
pub use texture::Texture;
pub use presentation::{SemaphorePresentation, FencePresentation};

// Simple offset_of macro akin to C++ offsetof
#[macro_export]
macro_rules! offset_of {
    ($base:path, $field:ident) => {{
        #[allow(unused_unsafe)]
        unsafe {
            let b: $base = mem::zeroed();
            (&b.$field as *const _ as isize) - (&b as *const _ as isize)
        }
    }};
}

pub fn as_byte_slice<T: Sized>(p: &T) -> &[u8] {
    unsafe {
        std::slice::from_raw_parts((p as *const T) as *const u8, ::std::mem::size_of::<T>())
    }
}

