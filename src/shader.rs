use super::context::Context;
use erupt::vk1_0::*;

use std::path::Path;
use std::sync::Arc;
use std::{io, slice};

pub struct Shader {
    shader_module: erupt::vk1_0::ShaderModule,
    ctx: Arc<Context>,
}

impl Shader {
    pub fn build(path: &Path, ctx: Arc<Context>) -> Shader {
        let shader = load_shader(path);
        let shader_module = unsafe {
            ctx.device
                .create_shader_module(
                    &ShaderModuleCreateInfoBuilder::new().code(&shader),
                    None,
                    None,
                )
                .expect("Shader module error")
        };

        Shader {
            shader_module,
            ctx: ctx.clone(),
        }
    }

    pub fn module(&self) -> erupt::vk1_0::ShaderModule {
        self.shader_module
    }
}

impl Drop for Shader {
    fn drop(&mut self) {
        unsafe {
            self.ctx.wait_idle();
            self.ctx
                .device
                .destroy_shader_module(self.shader_module, None);
        }
    }
}

fn load_shader(shader_path: &Path) -> Vec<u32> {
    use std::fs::File;
    use std::io::Read;
    let mut shader_data =
        File::open(shader_path).expect(&format!("failed to read shader {:?}", shader_path));
    let mut buffer = Vec::new();
    shader_data
        .read_to_end(&mut buffer)
        .expect("Failed to load shader data");

    read_spv(&mut shader_data).expect("Failed to read vertex shader spv file")
}

fn read_spv<R: io::Read + io::Seek>(x: &mut R) -> io::Result<Vec<u32>> {
    let size = x.seek(io::SeekFrom::End(0))?;
    if size % 4 != 0 {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "input length not divisible by 4",
        ));
    }
    if size > usize::max_value() as u64 {
        return Err(io::Error::new(io::ErrorKind::InvalidData, "input too long"));
    }
    let words = (size / 4) as usize;
    let mut result = Vec::<u32>::with_capacity(words);
    x.seek(io::SeekFrom::Start(0))?;
    unsafe {
        x.read_exact(slice::from_raw_parts_mut(
            result.as_mut_ptr() as *mut u8,
            words * 4,
        ))?;
        result.set_len(words);
    }
    const MAGIC_NUMBER: u32 = 0x0723_0203;
    if !result.is_empty() && result[0] == MAGIC_NUMBER.swap_bytes() {
        for word in &mut result {
            *word = word.swap_bytes();
        }
    }
    if result.is_empty() || result[0] != MAGIC_NUMBER {
        return Err(io::Error::new(
            io::ErrorKind::InvalidData,
            "input missing SPIR-V magic number",
        ));
    }
    Ok(result)
}
