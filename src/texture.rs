use erupt::{
    utils::allocator::{Allocation, MemoryTypeFinder},
    vk1_0::*,
};

use super::context::Context;
use std::path::Path;
use std::sync::Arc;

pub struct Texture {
    memory: Allocation<erupt::vk1_0::Image>,
    image: erupt::vk1_0::Image,
    view: Option<ImageView>,
    sampler: Option<Sampler>,

    width: u32,
    height: u32,
    format: Format,
    ctx: Arc<Context>,
}

impl Texture {
    pub fn new(image_info: ImageCreateInfo, ctx: Arc<Context>) -> Self {
        let image = unsafe {
            ctx.device
                .create_image(&image_info, None, None)
                .expect("Failed to create Texture Image!")
        };
        let mut allocator = ctx.allocator.lock().unwrap();
        let memory = allocator
            .allocate(&ctx.device, image, MemoryTypeFinder::download())
            .unwrap();

        Self {
            image,
            memory,
            width: image_info.extent.width,
            height: image_info.extent.height,
            format: image_info.format,
            view: None,
            sampler: None,
            ctx: ctx.clone(),
        }
    }

    pub fn attach_view(&mut self, image_info: ImageViewCreateInfo) {
        self.view = Some(unsafe {
            self.ctx
                .device
                .create_image_view(&image_info, None, None)
                .expect("Failed to create Image View!")
        });
    }

    pub fn attach_sampler(&mut self, sampler_info: SamplerCreateInfo) {
        self.sampler = Some(unsafe {
            self.ctx
                .device
                .create_sampler(&sampler_info, None, None)
                .expect("Failed to create Image View!")
        });
    }

    pub fn image(&self) -> erupt::vk1_0::Image {
        self.image
    }

    pub fn view(&self) -> ImageView {
        self.view.expect("No image attached")
    }

    pub fn sampler(&self) -> Sampler {
        self.sampler.expect("No image attached")
    }

    pub fn format(&self) -> Format {
        self.format
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }

    pub fn build_image(
        texture: Texture,
        buffer: &Allocation<Buffer>,
        ctx: &Arc<Context>,
        pool: CommandPool,
    ) -> Texture {
        ctx.apply_pipeline_barrier(
            PipelineStageFlags::TOP_OF_PIPE,
            PipelineStageFlags::TRANSFER,
            ImageMemoryBarrierBuilder::new()
                .src_access_mask(AccessFlags::empty())
                .dst_access_mask(AccessFlags::TRANSFER_WRITE)
                .old_layout(ImageLayout::UNDEFINED)
                .new_layout(ImageLayout::TRANSFER_DST_OPTIMAL)
                .image(texture.image())
                .subresource_range(ImageSubresourceRange {
                    aspect_mask: ImageAspectFlags::COLOR,
                    base_mip_level: 0,
                    level_count: 1,
                    base_array_layer: 0,
                    layer_count: 1,
                }),
            pool,
        );

        let buffer_image_regions = BufferImageCopyBuilder::new()
            .image_subresource(ImageSubresourceLayers {
                aspect_mask: ImageAspectFlags::COLOR,
                mip_level: 0,
                base_array_layer: 0,
                layer_count: 1,
            })
            .image_extent(Extent3D {
                width: texture.width,
                height: texture.height,
                depth: 1,
            })
            .image_offset(Offset3D { x: 0, y: 0, z: 0 });

        ctx.copy_buffer_to_image(
            *buffer.object(),
            texture.image(),
            vec![buffer_image_regions],
            pool,
        );

        ctx.apply_pipeline_barrier(
            PipelineStageFlags::TRANSFER,
            PipelineStageFlags::FRAGMENT_SHADER,
            ImageMemoryBarrierBuilder::new()
                .src_access_mask(AccessFlags::TRANSFER_WRITE)
                .dst_access_mask(AccessFlags::SHADER_READ)
                .old_layout(ImageLayout::TRANSFER_DST_OPTIMAL)
                .new_layout(ImageLayout::SHADER_READ_ONLY_OPTIMAL)
                .image(texture.image())
                .subresource_range(ImageSubresourceRange {
                    aspect_mask: ImageAspectFlags::COLOR,
                    base_mip_level: 0,
                    level_count: 1,
                    base_array_layer: 0,
                    layer_count: 1,
                }),
            pool,
        );

        texture
    }

    pub fn from_file(image_path: &Path, ctx: &Arc<Context>, pool: CommandPool) -> Self {
        use image::GenericImageView;
        let mut image_object = image::open(image_path).expect("failed to open texture file!"); // this function is slow in debug mode.
        image_object = image_object.flipv();
        let (image_width, image_height) = (image_object.width(), image_object.height());
        let image_size =
            (std::mem::size_of::<u8>() as u32 * image_width * image_height * 4) as DeviceSize;
        let image_data = match &image_object {
            image::DynamicImage::ImageLuma8(_)
            | image::DynamicImage::ImageBgr8(_)
            | image::DynamicImage::ImageRgb8(_) => image_object.to_rgba().into_raw(),
            image::DynamicImage::ImageLumaA8(_)
            | image::DynamicImage::ImageBgra8(_)
            | image::DynamicImage::ImageRgba8(_) => image_object.raw_pixels(),
        };

        if image_size <= 0 {
            panic!("Failed to load texture image!")
        }

        let format = Format::R8G8B8A8_UNORM;

        let image_buffer = ctx.create_buffer(
            BufferCreateInfoBuilder::new()
                .sharing_mode(SharingMode::EXCLUSIVE)
                .usage(BufferUsageFlags::TRANSFER_SRC)
                .size(image_size as DeviceSize),
            image_data.as_slice(),
        );

        let image_info = ImageCreateInfoBuilder::new()
            .image_type(ImageType::_2D)
            .extent(Extent3D {
                width: image_width,
                height: image_height,
                depth: 1,
            })
            .usage(ImageUsageFlags::TRANSFER_DST | ImageUsageFlags::SAMPLED)
            .format(format)
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCountFlagBits::_1)
            .sharing_mode(SharingMode::EXCLUSIVE)
            .tiling(ImageTiling::OPTIMAL);

        let mut image = Texture::new(*image_info, ctx.clone());
        let view_info = ImageViewCreateInfoBuilder::new()
            .view_type(ImageViewType::_2D)
            .format(format)
            .image(image.image())
            .components(ComponentMapping {
                r: ComponentSwizzle::IDENTITY,
                g: ComponentSwizzle::IDENTITY,
                b: ComponentSwizzle::IDENTITY,
                a: ComponentSwizzle::IDENTITY,
            })
            .subresource_range(ImageSubresourceRange {
                aspect_mask: ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            });
        let sampler_info = SamplerCreateInfoBuilder::new()
            .mag_filter(Filter::LINEAR)
            .min_filter(Filter::LINEAR)
            .mipmap_mode(SamplerMipmapMode::LINEAR)
            .address_mode_u(SamplerAddressMode::REPEAT)
            .address_mode_v(SamplerAddressMode::REPEAT)
            .address_mode_w(SamplerAddressMode::REPEAT)
            .max_lod(1.0)
            .mip_lod_bias(0.0)
            .anisotropy_enable(false)
            .max_anisotropy(16.0);
        image.attach_view(*view_info);
        image.attach_sampler(*sampler_info);
        Self::build_image(image, &image_buffer, ctx, pool)
    }

    pub fn from_data(
        data: Vec<u8>,
        width: u32,
        height: u32,
        ctx: &Arc<Context>,
        pool: CommandPool,
    ) -> Self {
        let image_size = (std::mem::size_of::<u8>() as u32 * width * height * 4) as DeviceSize;
        let format = Format::R8G8B8A8_UNORM;
        let image_info = ImageCreateInfoBuilder::new()
            .image_type(ImageType::_2D)
            .extent(Extent3D {
                width: width,
                height: height,
                depth: 1,
            })
            .usage(ImageUsageFlags::TRANSFER_DST | ImageUsageFlags::SAMPLED)
            .format(format)
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCountFlagBits::_1)
            .sharing_mode(SharingMode::EXCLUSIVE)
            .tiling(ImageTiling::OPTIMAL);

        let mut image = Texture::new(*image_info, ctx.clone());

        let image_buffer = ctx.create_buffer(
            BufferCreateInfoBuilder::new()
                .sharing_mode(SharingMode::EXCLUSIVE)
                .usage(BufferUsageFlags::TRANSFER_SRC)
                .size(image_size as DeviceSize),
            data.as_slice(),
        );

        let view_info = ImageViewCreateInfoBuilder::new()
            .view_type(ImageViewType::_2D)
            .format(format)
            .image(image.image())
            .components(ComponentMapping {
                r: ComponentSwizzle::IDENTITY,
                g: ComponentSwizzle::IDENTITY,
                b: ComponentSwizzle::IDENTITY,
                a: ComponentSwizzle::IDENTITY,
            })
            .subresource_range(ImageSubresourceRange {
                aspect_mask: ImageAspectFlags::COLOR,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            });
        let sampler_info = SamplerCreateInfoBuilder::new()
            .mag_filter(Filter::LINEAR)
            .min_filter(Filter::LINEAR)
            .mipmap_mode(SamplerMipmapMode::LINEAR)
            .address_mode_u(SamplerAddressMode::REPEAT)
            .address_mode_v(SamplerAddressMode::REPEAT)
            .address_mode_w(SamplerAddressMode::REPEAT)
            .max_lod(1.0)
            .mip_lod_bias(0.0)
            .anisotropy_enable(false)
            .max_anisotropy(16.0);
        image.attach_view(*view_info);
        image.attach_sampler(*sampler_info);

        Self::build_image(image, &image_buffer, ctx, pool)
    }

    pub fn create_depth_image(context: &Arc<Context>) -> Texture {
        let depth_format = context.find_depth_format(
            &[
                Format::D32_SFLOAT,
                Format::D32_SFLOAT_S8_UINT,
                Format::D24_UNORM_S8_UINT,
            ],
            ImageTiling::OPTIMAL,
            FormatFeatureFlags::DEPTH_STENCIL_ATTACHMENT,
        );
        let image_info = ImageCreateInfoBuilder::new()
            .image_type(ImageType::_2D)
            .extent(Extent3D {
                width: context.extent().width,
                height: context.extent().height,
                depth: 1,
            })
            .usage(ImageUsageFlags::DEPTH_STENCIL_ATTACHMENT | ImageUsageFlags::SAMPLED)
            .format(depth_format)
            .mip_levels(1)
            .array_layers(1)
            .samples(SampleCountFlagBits::_1)
            .sharing_mode(SharingMode::EXCLUSIVE)
            .tiling(ImageTiling::OPTIMAL);

        let mut texture = Texture::new(*image_info, context.clone());

        let view_info = ImageViewCreateInfoBuilder::new()
            .view_type(ImageViewType::_2D)
            .format(depth_format)
            .image(texture.image())
            .components(ComponentMapping {
                r: ComponentSwizzle::IDENTITY,
                g: ComponentSwizzle::IDENTITY,
                b: ComponentSwizzle::IDENTITY,
                a: ComponentSwizzle::IDENTITY,
            })
            .subresource_range(ImageSubresourceRange {
                aspect_mask: ImageAspectFlags::DEPTH,
                base_mip_level: 0,
                level_count: 1,
                base_array_layer: 0,
                layer_count: 1,
            });

        texture.attach_view(*view_info);

        let sampler_info = SamplerCreateInfoBuilder::new()
            .mag_filter(Filter::LINEAR)
            .min_filter(Filter::LINEAR)
            .mipmap_mode(SamplerMipmapMode::LINEAR)
            .address_mode_u(SamplerAddressMode::REPEAT)
            .address_mode_v(SamplerAddressMode::REPEAT)
            .address_mode_w(SamplerAddressMode::REPEAT)
            .max_lod(1.0)
            .mip_lod_bias(0.0)
            .anisotropy_enable(false)
            .max_anisotropy(16.0);

        texture.attach_sampler(*sampler_info);

        texture
    }
}

impl Drop for Texture {
    fn drop(&mut self) {
        use erupt::utils::allocator::AllocationObject;
        unsafe {
            self.memory.object().destroy(&self.ctx.device);
            self.image.destroy(&self.ctx.device);
        }
    }
}
