use erupt::{
    cstr,
    extensions::{ext_debug_utils::*, khr_surface::*, khr_swapchain::*},
    utils::{
        allocator::{Allocation, Allocator, AllocatorCreateInfo, MemoryTypeFinder},
        loading::DefaultCoreLoader,
        surface,
    },
    vk1_0::*,
    CoreLoader, DeviceLoader, InstanceLoader,
};
use std::{
    ffi::{c_void, CStr, CString},
    os::raw::c_char,
    sync::{Arc, Mutex},
};
use structopt::StructOpt;

const LAYER_KHRONOS_VALIDATION: *const c_char = cstr!("VK_LAYER_KHRONOS_validation");

#[derive(Debug, StructOpt)]
struct Opt {
    /// Use validation layers
    #[structopt(short, long)]
    debug: bool,
}

unsafe extern "system" fn debug_callback(
    _message_severity: DebugUtilsMessageSeverityFlagBitsEXT,
    _message_types: DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> Bool32 {
    eprintln!(
        "{}",
        CStr::from_ptr((*p_callback_data).p_message).to_string_lossy()
    );

    FALSE
}

pub struct Context {
    pub core: DefaultCoreLoader,
    pub device: DeviceLoader,
    pub physical_device: PhysicalDevice,
    pub instance: InstanceLoader,
    pub surface_capabilities: SurfaceCapabilitiesKHR,
    pub surface: SurfaceKHR,
    pub render_queue: Queue,
    pub compute_queue: Queue,
    pub graphics_queue_index: u32,
    pub compute_queue_index: u32,
    pub image_count: u32,
    pub present_mode: PresentModeKHR,
    pub format: SurfaceFormatKHR,
    pub messenger: DebugUtilsMessengerEXT,
    pub allocator: Arc<Mutex<Allocator>>,
}

#[allow(dead_code)]
impl Context {
    pub fn new(name: &str, window: &winit::window::Window) -> Context {
        let opt = Opt::from_args();

        let mut core = CoreLoader::new().unwrap();
        core.load_vk1_0().unwrap();

        let api_version = core.instance_version();
        println!(
            "{}- Vulkan {}.{}.{}",
            name,
            erupt::version_major(api_version),
            erupt::version_minor(api_version),
            erupt::version_patch(api_version)
        );

        // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Instance
        let application_name = CString::new(name).unwrap();
        let engine_name = CString::new("Vulkan Engine").unwrap();
        let app_info = ApplicationInfoBuilder::new()
            .application_name(&application_name)
            .application_version(erupt::make_version(1, 0, 0))
            .engine_name(&engine_name)
            .engine_version(erupt::make_version(1, 0, 0))
            .api_version(erupt::make_version(1, 0, 0));

        let mut instance_extensions = surface::enumerate_required_extensions(window).unwrap();

        let mut instance_layers = Vec::new();
        let mut device_layers = Vec::new();
        if true {
            println!(
                "Using vulkan validation layers {:?}",
                LAYER_KHRONOS_VALIDATION
            );
            instance_layers.push(LAYER_KHRONOS_VALIDATION);
            device_layers.push(LAYER_KHRONOS_VALIDATION);
            instance_extensions.push(EXT_DEBUG_UTILS_EXTENSION_NAME);
        }

        let device_extensions = vec![KHR_SWAPCHAIN_EXTENSION_NAME];
        let create_info = InstanceCreateInfoBuilder::new()
            .application_info(&app_info)
            .enabled_extension_names(&instance_extensions)
            .enabled_layer_names(&instance_layers);

        let mut instance = InstanceLoader::new(
            &core,
            unsafe { core.create_instance(&create_info, None, None) }.unwrap(),
        )
        .unwrap();
        instance.load_vk1_0().unwrap();

        // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/debug
        let messenger = if opt.debug {
            instance.load_ext_debug_utils().unwrap();

            let create_info = DebugUtilsMessengerCreateInfoEXTBuilder::new()
                .message_severity(
                    DebugUtilsMessageSeverityFlagsEXT::VERBOSE_EXT
                        | DebugUtilsMessageSeverityFlagsEXT::WARNING_EXT
                        | DebugUtilsMessageSeverityFlagsEXT::ERROR_EXT,
                )
                .message_type(
                    DebugUtilsMessageTypeFlagsEXT::GENERAL_EXT
                        | DebugUtilsMessageTypeFlagsEXT::VALIDATION_EXT
                        | DebugUtilsMessageTypeFlagsEXT::PERFORMANCE_EXT,
                )
                .pfn_user_callback(Some(debug_callback));

            unsafe { instance.create_debug_utils_messenger_ext(&create_info, None, None) }.unwrap()
        } else {
            Default::default()
        };

        // https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Window_surface
        let surface = unsafe { surface::create_surface(&mut instance, window, None) }.unwrap();

        // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Physical_devices_and_queue_families
        let (physical_device, format, present_mode, properties) =
            unsafe { instance.enumerate_physical_devices(None) }
                .unwrap()
                .into_iter()
                .filter_map(|physical_device| unsafe {
                    let formats = instance
                        .get_physical_device_surface_formats_khr(physical_device, surface, None)
                        .unwrap();
                    let format = match formats
                        .iter()
                        .find(|surface_format| {
                            surface_format.format == Format::B8G8R8A8_SRGB
                                && surface_format.color_space == ColorSpaceKHR::SRGB_NONLINEAR_KHR
                        })
                        .and_then(|_| formats.get(0))
                    {
                        Some(surface_format) => surface_format.clone(),
                        None => return None,
                    };

                    let present_mode = instance
                        .get_physical_device_surface_present_modes_khr(
                            physical_device,
                            surface,
                            None,
                        )
                        .unwrap()
                        .into_iter()
                        .find(|present_mode| present_mode == &PresentModeKHR::MAILBOX_KHR)
                        .unwrap_or(PresentModeKHR::FIFO_KHR);

                    let supported_extensions = instance
                        .enumerate_device_extension_properties(physical_device, None, None)
                        .unwrap();
                    if !device_extensions.iter().all(|device_extension| {
                        let device_extension = CStr::from_ptr(*device_extension);

                        supported_extensions.iter().any(|properties| {
                            CStr::from_ptr(properties.extension_name.as_ptr()) == device_extension
                        })
                    }) {
                        return None;
                    }

                    let properties = instance.get_physical_device_properties(physical_device, None);
                    Some((physical_device, format, present_mode, properties))
                })
                .max_by_key(|(_, _, _, properties)| match properties.device_type {
                    PhysicalDeviceType::DISCRETE_GPU => 2,
                    PhysicalDeviceType::INTEGRATED_GPU => 1,
                    _ => 0,
                })
                .expect("No suitable physical device found");

        println!("Using physical device: {:?}", unsafe {
            CStr::from_ptr(properties.device_name.as_ptr())
        });

        let mut graphics_queue_index: u32 = 0;
        let mut compute_queue_index: u32 = 0;

        unsafe {
            instance
                .get_physical_device_queue_family_properties(physical_device, None)
                .into_iter()
                .enumerate()
                .for_each(|(index, properties)| {
                    let is_present_support = instance
                        .get_physical_device_surface_support_khr(
                            physical_device,
                            index as u32,
                            surface,
                            None,
                        )
                        .unwrap();

                    if properties.queue_flags.contains(QueueFlags::GRAPHICS)
                        && is_present_support == true
                    {
                        graphics_queue_index = index as u32;
                    }

                    if properties.queue_flags.contains(QueueFlags::COMPUTE) {
                        compute_queue_index = index as u32;
                    }
                })
        };

        // https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Logical_device_and_queues
        let queue_create_info = vec![
            DeviceQueueCreateInfoBuilder::new()
                .queue_family_index(graphics_queue_index)
                .queue_priorities(&[1.0]),
            DeviceQueueCreateInfoBuilder::new()
                .queue_family_index(compute_queue_index)
                .queue_priorities(&[1.0]),
        ];

        let features = PhysicalDeviceFeaturesBuilder::new();

        let device_create_info = DeviceCreateInfoBuilder::new()
            .queue_create_infos(&queue_create_info)
            .enabled_features(&features)
            .enabled_extension_names(&device_extensions)
            .enabled_layer_names(&device_layers);

        let mut device = DeviceLoader::new(
            &instance,
            unsafe { instance.create_device(physical_device, &device_create_info, None, None) }
                .unwrap(),
        )
        .unwrap();
        device.load_vk1_0().unwrap();
        device.load_khr_swapchain().unwrap();

        let render_queue = unsafe { device.get_device_queue(graphics_queue_index, 0, None) };
        let compute_queue = unsafe { device.get_device_queue(compute_queue_index, 0, None) };

        // https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Swap_chain
        let surface_capabilities = unsafe {
            instance.get_physical_device_surface_capabilities_khr(physical_device, surface, None)
        }
        .unwrap();
        let mut image_count = surface_capabilities.min_image_count + 1;
        if surface_capabilities.max_image_count > 0
            && image_count > surface_capabilities.max_image_count
        {
            image_count = surface_capabilities.max_image_count;
        }

        let allocator =
            Allocator::new(&instance, physical_device, AllocatorCreateInfo::default()).unwrap();

        Context {
            core,
            device,
            instance,
            physical_device,
            render_queue,
            compute_queue,
            graphics_queue_index,
            compute_queue_index,
            surface_capabilities,
            format,
            present_mode,
            surface,
            image_count,
            messenger,
            allocator: Arc::new(Mutex::new(allocator)),
        }
    }

    pub fn get_ubo_alignment<T>(&self) -> u32 {
        let min_alignment = unsafe {
            self.instance
                .get_physical_device_properties(self.physical_device, None)
                .limits
                .min_uniform_buffer_offset_alignment as u32
        };
        let t_size = std::mem::size_of::<T>() as u32;

        if t_size <= min_alignment {
            min_alignment
        } else {
            min_alignment * (t_size as f32 / min_alignment as f32).ceil() as u32
        }
    }

    pub fn get_storage_buffer_alignment<T>(&self) -> u32 {
        let min_alignment = unsafe {
            self.instance
                .get_physical_device_properties(self.physical_device, None)
                .limits
                .min_storage_buffer_offset_alignment as u32
        };
        let t_size = std::mem::size_of::<T>() as u32;

        if t_size <= min_alignment {
            min_alignment
        } else {
            min_alignment * (t_size as f32 / min_alignment as f32).ceil() as u32
        }
    }

    pub fn create_allocator(&self) -> Allocator {
        Allocator::new(
            &self.instance,
            self.physical_device,
            AllocatorCreateInfo::default(),
        )
        .unwrap()
    }

    pub fn wait_idle(&self) {
        unsafe {
            self.device.device_wait_idle().unwrap();
        }
    }

    pub fn format(&self) -> Format {
        self.format.format
    }

    pub fn extent(&self) -> Extent2D {
        self.surface_capabilities.current_extent
    }

    pub fn create_command_pool(&self, info: CommandPoolCreateInfoBuilder) -> CommandPool {
        unsafe { self.device.create_command_pool(&info, None, None) }.unwrap()
    }

    pub fn create_command_buffers(
        &self,
        info: CommandBufferAllocateInfoBuilder,
    ) -> Vec<CommandBuffer> {
        unsafe { self.device.allocate_command_buffers(&info) }.unwrap()
    }

    pub fn create_buffer(
        &self,
        info: BufferCreateInfoBuilder,
        data: &[u8],
    ) -> Allocation<erupt::vk1_0::Buffer> {
        let mut allocator = self.allocator.lock().unwrap();
        let buffer = allocator
            .allocate(
                &self.device,
                unsafe { self.device.create_buffer(&info, None, None) }.unwrap(),
                MemoryTypeFinder::download(),
            )
            .unwrap();
        let mut map = buffer
            .map(&self.device, ..buffer.region().start + info.size)
            .unwrap();

        map.import(&data);
        map.unmap(&self.device).unwrap();
        buffer
    }

    pub fn update_buffer(&self, buffer: &Allocation<erupt::vk1_0::Buffer>, data: Vec<u8>) {
        let mut map = buffer
            .map(
                &self.device,
                ..buffer.region().start + buffer.region().end as u64,
            )
            .unwrap();

        map.import(&data);
        map.unmap(&self.device).unwrap();
    }

    pub fn begin_single_time_command(&self, pool: CommandPool) -> CommandBuffer {
        let command_buffer = self.create_command_buffers(
            CommandBufferAllocateInfoBuilder::new()
                .command_pool(pool)
                .level(CommandBufferLevel::PRIMARY)
                .command_buffer_count(1),
        )[0];

        let begin_info = CommandBufferBeginInfoBuilder::new();
        unsafe {
            self.device
                .begin_command_buffer(command_buffer, &begin_info)
        }
        .unwrap();

        command_buffer
    }

    pub fn end_single_time_command(&self, command_buffer: CommandBuffer, pool: CommandPool) {
        unsafe {
            self.device
                .end_command_buffer(command_buffer)
                .expect("Failed to record Command Buffer at Ending!");
        }
        let submit_buffer = vec![command_buffer];
        let submit_info = SubmitInfoBuilder::new().command_buffers(&submit_buffer);

        unsafe {
            self.device
                .queue_submit(self.render_queue, &[submit_info], Fence::null())
                .expect("Failed to Queue Submit!");
            self.device
                .queue_wait_idle(self.render_queue)
                .expect("Failed to wait Queue idle!");
            self.device.free_command_buffers(pool, &submit_buffer);
        }
    }

    pub fn copy_buffer_to_image(
        &self,
        buffer: Buffer,
        image: Image,
        image_regions: Vec<BufferImageCopyBuilder>,
        pool: CommandPool,
    ) {
        let command_buffer = self.begin_single_time_command(pool);
        unsafe {
            self.device.cmd_copy_buffer_to_image(
                command_buffer,
                buffer,
                image,
                ImageLayout::TRANSFER_DST_OPTIMAL,
                &image_regions,
            );
        }

        self.end_single_time_command(command_buffer, pool);
    }

    pub fn apply_pipeline_barrier(
        &self,
        src_stage: PipelineStageFlags,
        dst_stage: PipelineStageFlags,
        barrier: ImageMemoryBarrierBuilder,
        pool: CommandPool,
    ) {
        let command_buffer = self.begin_single_time_command(pool);
        unsafe {
            self.device.cmd_pipeline_barrier(
                command_buffer,
                src_stage,
                dst_stage,
                DependencyFlags::empty(),
                &[],
                &[],
                &[barrier],
            );
        }
        self.end_single_time_command(command_buffer, pool);
    }

    pub fn find_depth_format(
        &self,
        candidate_formats: &[Format],
        tiling: ImageTiling,
        features: FormatFeatureFlags,
    ) -> Format {
        for &format in candidate_formats.iter() {
            let format_properties = unsafe {
                self.instance.get_physical_device_format_properties(
                    self.physical_device,
                    format,
                    None,
                )
            };
            if tiling == ImageTiling::LINEAR
                && format_properties.linear_tiling_features.contains(features)
            {
                return format.clone();
            } else if tiling == ImageTiling::OPTIMAL
                && format_properties.optimal_tiling_features.contains(features)
            {
                return format.clone();
            }
        }

        panic!("Failed to find supported format!")
    }
}

impl Drop for Context {
    fn drop(&mut self) {
        unsafe {
            self.device.device_wait_idle().unwrap();
            self.device.destroy_device(None);
            self.instance.destroy_surface_khr(self.surface, None);
            if !self.messenger.is_null() {
                self.instance
                    .destroy_debug_utils_messenger_ext(self.messenger, None);
            }
            self.instance.destroy_instance(None);
        }
    }
}
