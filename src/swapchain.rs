use erupt::{
    extensions::{khr_surface::*, khr_swapchain::*},
    vk1_0::*,
};

use super::context::Context;
use std::sync::Arc;

pub struct Swapchain {
    swapchain: SwapchainKHR,
    swapchain_image_views: Vec<ImageView>,
    ctx: Arc<Context>,
}

impl Swapchain {
    pub fn new(ctx: Arc<Context>) -> Swapchain {
        let create_info = SwapchainCreateInfoKHRBuilder::new()
            .surface(ctx.surface)
            .min_image_count(ctx.image_count)
            .image_format(ctx.format.format)
            .image_color_space(ctx.format.color_space)
            .image_extent(ctx.surface_capabilities.current_extent)
            .image_array_layers(1)
            .image_usage(ImageUsageFlags::COLOR_ATTACHMENT)
            .image_sharing_mode(SharingMode::EXCLUSIVE)
            .pre_transform(ctx.surface_capabilities.current_transform)
            .composite_alpha(CompositeAlphaFlagBitsKHR::OPAQUE_KHR)
            .present_mode(ctx.present_mode)
            .clipped(true)
            .old_swapchain(SwapchainKHR::null());
        let swapchain =
            unsafe { ctx.device.create_swapchain_khr(&create_info, None, None) }.unwrap();
        let swapchain_images =
            unsafe { ctx.device.get_swapchain_images_khr(swapchain, None) }.unwrap();

        // https://vulkan-tutorial.com/Drawing_a_triangle/Presentation/Image_views
        let swapchain_image_views: Vec<_> = swapchain_images
            .iter()
            .map(|swapchain_image| {
                let create_info = ImageViewCreateInfoBuilder::new()
                    .image(*swapchain_image)
                    .view_type(ImageViewType::_2D)
                    .format(ctx.format.format)
                    .components(ComponentMapping {
                        r: ComponentSwizzle::IDENTITY,
                        g: ComponentSwizzle::IDENTITY,
                        b: ComponentSwizzle::IDENTITY,
                        a: ComponentSwizzle::IDENTITY,
                    })
                    .subresource_range(unsafe {
                        ImageSubresourceRangeBuilder::new()
                            .aspect_mask(ImageAspectFlags::COLOR)
                            .base_mip_level(0)
                            .level_count(1)
                            .base_array_layer(0)
                            .layer_count(1)
                            .discard()
                    });
                unsafe { ctx.device.create_image_view(&create_info, None, None) }.unwrap()
            })
            .collect();

        Swapchain {
            swapchain,
            swapchain_image_views,
            ctx: ctx.clone(),
        }
    }

    pub fn images(&self) -> Vec<ImageView> {
        self.swapchain_image_views.clone()
    }

    pub fn image_count(&self) -> u32 {
        self.swapchain_image_views.len() as u32
    }

    pub fn info(&self) -> SwapchainKHR {
        self.swapchain
    }
}

impl Drop for Swapchain {
    fn drop(&mut self) {
        unsafe {
            self.ctx.wait_idle();
            self.ctx.device.destroy_swapchain_khr(self.swapchain, None);
            for &image_view in &self.swapchain_image_views {
                self.ctx.device.destroy_image_view(image_view, None);
            }
        }
    }
}
