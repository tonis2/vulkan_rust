# vulkan_rust

An example projects how to run Vulkan api using Rust language.

To run this example make sure your graphics card supports Vulkan and you have installed [Cargo and Rust](https://doc.rust-lang.org/cargo/getting-started/installation.html)

To run examples use

`cargo run --example example_name`


Includes somewhat working `GLTF` loader and key event parser.

In order to move around 3D running scenes press and hold right mouse button.


What examples contain:

Triangle

![triangle](images/triangle.png)

Texture

![texture](images/texture.png)

Deferred, rendering with lightning

![texture](images/deferred.png)
